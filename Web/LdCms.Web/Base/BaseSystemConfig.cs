﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace LdCms.Web
{
    /// <summary>
    /// 系统配置参数
    /// </summary>
    public static class BaseSystemConfig
    {
        public static int SystemID = 100101;                    //系统编号
        public static int AccessTokenExpiresIn = 7200;          //token过期时间：  7200秒    2小时
        public static int RefreshTokenExpiresIn = 2592000;      //refresh过期时间：2592000秒 30天
        public static string SessionName = ".LdCmsNetCore";     //Session名称
        public static string SiteConfigFile = HttpContext.Current.Server.MapPath("~/app_data/config/siteconfig.json");


        public static string HeadPortraitImage = "/admin/static/h-ui.admin/images/head_image.jpg";

        public static string WeChatServerNotifyUrl = "cgi/v2/wechat/server/notify.action";
        public static string WeixinPayNotifyUrl = "cgi/v2/wechat/pay/notify.action";
        public static string WeixinPayRefundNotifyUrl = "cgi/v2/wechat/pay/refund/notify.action";

        public static string AlipayServerCheckUrl = "cgi/v2/alipay/server/gateway.action";
        public static string AlipayTradeNotifyUrl = "cgi/v2/alipay/trade/notify.action";

        public static string UnionpayAggregatePayTradeNotifyUrl = "cgi/v2/alipay/trade/notify.action";
    }
}
