﻿using System;
using System.Web;
using System.Web.Mvc;

namespace LdCms.Web.Controllers
{
    using LdCms.Common.Web;
    using LdCms.Common.Security;
    using LdCms.Common.VerifyCode;
    /// <summary>
    /// 证验码
    /// </summary>
    public class VerifyCodeController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                string randomCode = GeneralCodeHelper.GetRandomString(5);
                string encryptCode = DESEncryptHelper.EncryptDES(randomCode);
                WebHelper.WriteCookie("VerifyCode", encryptCode, 30);
                var imageByte = VerifyCodeHelper.Create(randomCode);
                return File(imageByte, @"image/png");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [ActionName("get")]
        public string GetVerifyCode(string cdoe)
        {
            try
            {
                string decodeCode = HttpUtility.UrlDecode(cdoe);
                string verifyCode = WebHelper.GetCookie("VerifyCode");
                return DESEncryptHelper.DecryptDES(verifyCode);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}