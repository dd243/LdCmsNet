﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Web.Hosting;

namespace LdCms.Web.Controllers
{
    using LdCms.EF.DbContext;
    using LdCms.EF.DbModels;
    using LdCms.Common.Json;
    using LdCms.Web.Services;
    using LdCms.IBLL.Basics;
    using LdCms.Common.Extension;
    using LdCms.IBLL.Sys;
    using LdCms.Web.Models;
    using LdCms.Common.Net;

    public class HomeController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly IConfigService ConfigService;
        public HomeController(IBaseManager BaseManager, IConfigService ConfigService) :base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.ConfigService = ConfigService;
        }
        public override ActionResult Index()
        {
            try
            {
                int systemId = BaseSystemConfig.SystemID;
                string companyId = SiteConfig.CompanyID;
                var entity = ConfigService.GetConfig(systemId, companyId);
                string homeUrl = string.IsNullOrEmpty(entity.HomeUrl) ? SiteConfig.HomeUrl : entity.HomeUrl;
                return Redirect(homeUrl);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ActionResult Error()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult s()
        {
            return View();
        }


    }



}
