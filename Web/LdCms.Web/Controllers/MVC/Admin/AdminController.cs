﻿using System;
using System.Web;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Admin
{
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    using LdCms.Common.Net;
    using LdCms.Common.Security;
    using LdCms.Common.Utility;
    using LdCms.Common.Web;
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Institution;
    using LdCms.IBLL.Log;
    using LdCms.IBLL.Info;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    

    /// <summary>
    /// 后台管理主页控制器
    /// </summary>
    //[ControllerName("admin")]
    public class AdminController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly IStaffService StaffService;
        private readonly ILoginRecordService LoginRecordService;
        private readonly INoticeService NoticeService;
        public AdminController(IBaseManager BaseManager, IStaffService StaffService, ILoginRecordService LoginRecordService, INoticeService NoticeService) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.StaffService = StaffService;
            this.LoginRecordService = LoginRecordService;
            this.NoticeService = NoticeService;
        }
        /// <summary>
        /// 管理后台首页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("index")]
        [AdminAuthorize(Roles = "Admins")]
        public override ActionResult Index()
        {
            try
            {
                ViewBag.CompanyID = CompanyID;
                ViewBag.StaffID = StaffID;
                ViewBag.StaffName = StaffName;
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 后台欢迎主页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("welcome")]
        [AdminAuthorize(Roles = "Admins")]
        public ActionResult Welcome()
        {
            try
            {

                var listLoginRecord = LoginRecordService.GetLoginRecordTop(SystemID, CompanyID, 8);
                var listInfoNotice = NoticeService.GetNoticeTop(SystemID, CompanyID, 8);

                ViewBag.LoginRecord = listLoginRecord;
                ViewBag.InfoNotice = listInfoNotice;
                ViewBag.ServerMachineName = Utility.GetServerMachineName();
                ViewBag.SystemVersion = Utility.GetSystemVersion();
                ViewBag.ServerIpAddress = System.Web.HttpContext.Current.Request.GetRemoteIpAddress();
                ViewBag.HttpWebHost = System.Web.HttpContext.Current.Request.GetHttpWebHost();
                ViewBag.HttpWebPort = System.Web.HttpContext.Current.Request.GetHttpWebPort();
                ViewBag.SystemVersion = Utility.GetSystemVersion();
                ViewBag.NetCoreVersion = Utility.GetNetCoreVersion();
                ViewBag.CompanyName = CompanyName;
                ViewBag.StaffName = StaffName;

                return View();
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }
        /// <summary>
        /// 退出登录处理
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("logout")]
        [AdminAuthorize(Roles = "Admins")]
        public ActionResult Logout()
        {
            try
            {
                SaveLoginRecord(CompanyID, StaffID, StaffName, true, 2);
                WebHelper.RemoveCookie(SessionName);
                return RedirectToAction("login","admin", new { companyid = CompanyID });
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }

        #region 登录部分
        /// <summary>
        /// 登录页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("login")]
        [AdminAuthorize(Validate = false)]
        public ActionResult Login()
        {
            try
            {
                string companyId = GetQueryString("companyid");
                if (string.IsNullOrWhiteSpace(companyId))
                    ViewBag.CompanyID = SiteConfig.CompanyID;
                else
                    ViewBag.CompanyID = companyId;
                string sessionJson = WebHelper.GetCookie(SessionName);
                if (!string.IsNullOrEmpty(sessionJson))
                {
                    AccountModel loginStaffModel = DESEncryptHelper.DecryptDES(sessionJson).ToObject<AccountModel>();
                    if (loginStaffModel.Online)
                        return RedirectToAction("index", "admin");
                }
                return View(SiteConfig);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }
        /// <summary>
        /// 处理登录请求
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize(Validate = false)]
        public JsonResult Login(string companyId)
        {
            try
            {
                string username = GetFormValue("username");
                string password = GetFormValue("password");
                string verifyCode = GetFormValue("verifycode");
                bool online = GetFormValue("online").ToBool();
                string cookieVerifyCode = WebHelper.GetCookie("VerifyCode");
                string decryptCookieVerifyCode = DESEncryptHelper.DecryptDES(cookieVerifyCode);

                string[] arrUserName = username.Split('@');
                if (arrUserName.Length == 2)
                {
                    companyId = arrUserName[0].ToString();
                    username = arrUserName[1].ToString();
                }
                else
                {
                    companyId = Utility.IIF(companyId, "sys");
                }

                if (string.IsNullOrEmpty(username))
                    return Error("用户名不能为空！");
                if (string.IsNullOrEmpty(password))
                    return Error("密码不能为空！");
                if (string.IsNullOrEmpty(verifyCode))
                    return Error("验证码不能为空！");
                if (decryptCookieVerifyCode.ToUpper() != verifyCode.ToUpper())
                    return Error("验证码不正确！");

                var LoginResult = StaffService.VerifyStaffLoginPro(SystemID, companyId, username, AlgorithmHelper.MD5(password));
                if (LoginResult)
                {
                    var entityStaff = StaffService.GetVStaffPro(SystemID, companyId, username);
                    string staffId = entityStaff.StaffID;
                    string staffName = entityStaff.StaffName;
                    string CompanyId = entityStaff.CompanyID;
                    string CompanyName = entityStaff.CompanyName;
                    SaveLoginRecord(companyId, username, staffName, LoginResult, 1);
                    AccountModel entity = new AccountModel()
                    {
                        SessionID = "",
                        CompanyID = CompanyId,
                        CompanyName = CompanyName,
                        StaffID = username,
                        StaffName = staffName,
                        Online = online,
                        Roles = "Admins"
                    };
                    string userJson = DESEncryptHelper.EncryptDES(entity.ToJson());
                    WebHelper.WriteCookie(SessionName, userJson);
                    return Success("成功");
                }
                else
                {
                    SaveLoginRecord(companyId, username, "-", LoginResult, 2);
                    return Error("login fail");
                }
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        #endregion

        #region 私有方法
        public void SaveLoginRecord(string companyId, string account, string nickName, bool isResult, int loginType)
        {
            try
            {
                byte typeId = loginType.ToByte();
                string typeName = loginType == 1 ? "登录" : "退出";
                string description = loginType == 1 ? "登录管理后台" : "退出管理后台";
                var result = LoginRecordService.SaveLoginRecord(new Ld_Log_LoginRecord()
                {
                    SystemID = SystemID,
                    CompanyID = companyId,
                    TypeID = typeId,
                    TypeName = typeName,
                    Account = account,
                    NickName = nickName,
                    ClientID = 1,
                    ClientName = "Web",
                    IpAddress = Net.Ip,
                    Description = description,
                    IsResult = isResult
                });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion



    }
}