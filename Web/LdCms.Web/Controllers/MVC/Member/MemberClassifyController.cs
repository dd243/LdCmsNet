﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Member
{
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Member;
    using LdCms.Common.Extension;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    using LdCms.Common.Json;


    [AdminAuthorize(Roles = "Admins")]
    public class MemberClassifyController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly IClassifyService ClassifyService;
        private readonly ITableOperationManager<Ld_Member_Classify> TableOperationManager;
        public MemberClassifyController(IBaseManager BaseManager, IClassifyService ClassifyService, ITableOperationManager<Ld_Member_Classify> TableOperationManager) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.ClassifyService = ClassifyService;
            this.TableOperationManager = TableOperationManager;
            TableOperationManager.Account = StaffID;
            TableOperationManager.NickName = StaffName;
        }
        public override ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.列表);
                if (!IsPermission(funcId)) { return ToPermission(funcId); }
                List<Ld_Member_Classify> lists = ClassifyService.GetClassify(SystemID, CompanyID);
                ViewBag.Count = ClassifyService.CountClassify(SystemID, CompanyID);
                return View(lists);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }
        public ActionResult Add(string classId)
        {
            try
            {
                string funcId = string.Empty;
                if (!IsAddPermission(classId, out funcId))
                    return ToPermission(funcId);
                if (string.IsNullOrWhiteSpace(classId))
                    return View(new Ld_Member_Classify());
                var entity = ClassifyService.GetClassify(SystemID, CompanyID, classId.ToInt());
                return View(entity);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult Save(string classId)
        {
            try
            {
                if (!IsSavePermission(classId))
                    return Error("您没有操作权限，请联系系统管理员！");

                string fClassID = GetFormValue("fClassID");
                string fClassName = GetFormValue("fClassName");
                string fRemark = GetFormValue("fRemark");
                string fState = GetFormValue("fState");

                var entity = new Ld_Member_Classify()
                {
                    SystemID = SystemID,
                    CompanyID = CompanyID,
                    ClassID = fClassID.ToInt(),
                    ClassName = fClassName,
                    Remark = fRemark,
                    State = fState.ToBool()
                };

                bool result = false;
                if (classId.ToInt() == 0)
                    result = ClassifyService.SaveClassify(entity);
                else
                    result = ClassifyService.UpdateClassify(entity);
                if (result)
                    return Success("ok");
                else
                    return Error("fail");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public JsonResult UpdateState(int classId, bool state)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.审核);
                if (!IsPermission(funcId)) { return Error("您没有操作权限，请联系系统管理员！"); }

                var result = ClassifyService.UpdateClassifyState(SystemID, CompanyID, classId, state);
                if (result)
                    return Success("ok");
                else
                    return Error("fail");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public JsonResult Delete(int classId)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");

                var result = ClassifyService.DeleteClassify(SystemID, CompanyID, classId);
                if (result)
                    return Success("ok");
                else
                    return Error("fail");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public JsonResult BatchDelete(int[] arrId)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                if (arrId.Length == 0)
                    return Error("请选择删除ID!");
                bool result = ClassifyService.DeleteClassify(SystemID, CompanyID, arrId);
                if (result)
                    return Success("ok");
                else
                    return Error("fail");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        [HttpGet]
        [ActionName("GetClassifyByState")]
        public JsonResult GetClassifyByState(string state)
        {
            try
            {
                var lists = ClassifyService.GetClassify(SystemID, CompanyID, state.ToBool());
                if (lists == null)
                    return Error("not data");
                var data = from m in lists
                           select new
                           {
                               id = m.ClassID,
                               name = m.ClassName
                           };
                return Success("ok", data);
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        public bool IsSavePermission(string classId)
        {
            try
            {

                if (classId.ToInt() == 0)
                {
                    string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.新增);
                    return IsPermission(funcId) ? true : false;
                }
                else
                {
                    string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.编辑);
                    return IsPermission(funcId) ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IsAddPermission(string classId, out string funcId)
        {
            try
            {
                if (classId.ToInt() == 0)
                {
                    funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.新增);
                    return IsPermission(funcId) ? true : false;
                }
                else
                {
                    funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.类别管理.编辑);
                    return IsPermission(funcId) ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}