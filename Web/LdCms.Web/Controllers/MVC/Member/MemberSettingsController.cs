﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Member
{
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Member;
    using LdCms.Common.Extension;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    using LdCms.Common.Json;
    /// <summary>
    /// 
    /// </summary>
    [AdminAuthorize(Roles = "Admins")]
    public class MemberSettingsController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly ISettingsService SettingsService;
        private readonly ITableOperationManager<Ld_Member_Settings> TableOperationManager;
        public MemberSettingsController(IBaseManager BaseManager, ISettingsService SettingsService, ITableOperationManager<Ld_Member_Settings> TableOperationManager) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.SettingsService = SettingsService;
            this.TableOperationManager = TableOperationManager;
            TableOperationManager.Account = StaffID;
            TableOperationManager.NickName = StaffName;
        }

        public override ActionResult Index()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.设置管理.查看);
                if (!IsPermission(funcId))
                    return ToPermission(funcId);

                var entity = SettingsService.GetSettings(SystemID, CompanyID);
                if (entity == null)
                    return View(new Ld_Member_Settings());
                return View(entity);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }

        [HttpPost]
        [ActionName("update")]
        public JsonResult SaveConfig()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.会员管理.设置管理.修改);
                if (!IsPermission(funcId)) { return Error("您没有操作权限，请联系系统管理员！"); }

                string fIsRegister = GetFormValue("fIsRegister");
                string fIsSmsCode = GetFormValue("fIsSmsCode");
                string fIsPoint = GetFormValue("fIsPoint");
                string fLoginPoint = GetFormValue("fLoginPoint");
                string fNewUserPoint = GetFormValue("fNewUserPoint");
                string fSignINPoint = GetFormValue("fSignINPoint");
                string fOrderPointType = GetFormValue("fOrderPointType");
                string fOrderPoint = GetFormValue("fOrderPoint");
                string fCommentPoint = GetFormValue("fCommentPoint");
                string fRemark = GetFormValue("fRemark");
                var result = SettingsService.UpdateSettings(new Ld_Member_Settings()
                {
                    SystemID = SystemID,
                    CompanyID = CompanyID,
                    IsRegister = fIsRegister.ToBool(),
                    IsSmsCode = fIsSmsCode.ToBool(),
                    IsPoint = fIsPoint.ToBool(),
                    NewUserPoint = fNewUserPoint.ToInt(),
                    LoginPoint = fLoginPoint.ToInt(),
                    SignINPoint = fSignINPoint.ToInt(),
                    OrderPointType = fOrderPointType.ToInt(),
                    OrderPoint = fOrderPoint.ToInt(),
                    CommentPoint = fCommentPoint.ToInt(),
                    Account = StaffID,
                    NickName = StaffName,
                    Remark = fRemark,
                    CreateDate = DateTime.Now
                });
                if (result)
                    return Success("ok");
                else
                    return Error("fail");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }


    }
}