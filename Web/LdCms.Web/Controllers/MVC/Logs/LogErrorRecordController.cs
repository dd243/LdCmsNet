﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Logs
{
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Log;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    /// <summary>
    /// 
    /// </summary>
    [AdminAuthorize(Roles = "Admins")]
    public class LogErrorRecordController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly IErrorRecordService ErrorRecordService;
        public LogErrorRecordController(IBaseManager BaseManager, IErrorRecordService ErrorRecordService) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.ErrorRecordService = ErrorRecordService;
        }
        public override ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.列表);
                if (!IsPermission(funcId)) { return ToPermission(funcId); }
                    
                string startTime = GetQueryString("datemin");
                string endTime = GetQueryString("datemax");
                string clientId = GetQueryString("clientId");
                string keyword = GetQueryString("keyword");
                ViewBag.DateMin = startTime;
                ViewBag.DateMax = endTime;
                ViewBag.ClientID = clientId;
                ViewBag.Keyword = keyword;

                int total = 100;
                List<Ld_Log_ErrorRecord> lists = new List<Ld_Log_ErrorRecord>();
                if (string.IsNullOrWhiteSpace(keyword) && string.IsNullOrWhiteSpace(startTime))
                {
                    ViewBag.Count = ErrorRecordService.CountErrorRecord(SystemID);
                    lists = ErrorRecordService.GetErrorRecordTop(SystemID, total);
                }
                else
                {
                    ViewBag.Count = ErrorRecordService.CountErrorRecord(SystemID, startTime, endTime, clientId, keyword);
                    lists = ErrorRecordService.SearchErrorRecord(SystemID, startTime, endTime, clientId, keyword, total);
                }
                return View(lists);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }
        public ActionResult Show(long id)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.列表);
                if (!IsPermission(funcId)) { return Error("您没有操作权限，请联系系统管理员！"); }
                var entity = ErrorRecordService.GetErrorRecord(id);
                return View(entity);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }


        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                var entity = ErrorRecordService.GetErrorRecord(id);
                if (entity == null)
                    return Error("id not exists！");
                bool result = ErrorRecordService.DeleteErrorRecord(id);
                if (result)
                    return Success("成功！");
                else
                    return Error("失败！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult DeleteBatch(string[] arrId)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                if (arrId.Length == 0)
                    return Error("请选择删除ID!");
                foreach (var item in arrId)
                {
                    long id = Convert.ToInt64(item);
                    bool result = ErrorRecordService.DeleteErrorRecord(id);
                }
                return Success("成功！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult DeleteAll()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                bool result = ErrorRecordService.DeleteErrorRecord(SystemID);
                if (result)
                    return Success("成功！");
                else
                    return Error("失败！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }


    }
}