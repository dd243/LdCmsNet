﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Logs
{
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Log;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    /// <summary>
    /// 
    /// </summary>
    [AdminAuthorize(Roles = "Admins")]
    public class LogVisitorRecordController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly IVisitorRecordService VisitorRecordService;
        public LogVisitorRecordController(IBaseManager BaseManager, IVisitorRecordService VisitorRecordService) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.VisitorRecordService = VisitorRecordService;
        }
        public override ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.列表);
                if (!IsPermission(funcId))
                    return ToPermission(funcId);
                string startTime = GetQueryString("datemin");
                string endTime = GetQueryString("datemax");
                string clientId = GetQueryString("clientId");
                string keyword = GetQueryString("keyword");
                ViewBag.DateMin = startTime;
                ViewBag.DateMax = endTime;
                ViewBag.ClientID = clientId;
                ViewBag.Keyword = keyword;

                int total = 100;
                List<Ld_Log_VisitorRecord> lists = new List<Ld_Log_VisitorRecord>();
                if (string.IsNullOrWhiteSpace(keyword) && string.IsNullOrWhiteSpace(startTime))
                    lists = VisitorRecordService.GetVisitorRecordTop(SystemID, CompanyID, total);
                else
                    lists = VisitorRecordService.SearchVisitorRecord(SystemID, CompanyID, startTime, endTime, clientId, keyword, total);
                ViewBag.Count = VisitorRecordService.CountVisitorRecord(SystemID, CompanyID);
                return View(lists);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                var entity = VisitorRecordService.GetVisitorRecord(id);
                if (entity == null)
                    return Error("id not exists！");
                bool result = VisitorRecordService.DeleteVisitorRecord(id);
                if (result)
                    return Success("成功！");
                else
                    return Error("失败！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult DeleteBatch(string[] arrId)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                if (arrId.Length == 0)
                    return Error("请选择删除ID!");
                foreach (var item in arrId)
                {
                    long id = Convert.ToInt64(item);
                    bool result = VisitorRecordService.DeleteVisitorRecord(id);
                }
                return Success("成功！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult DeleteAll()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.日志管理.系统日志.删除);
                if (!IsPermission(funcId))
                    return Error("您没有操作权限，请联系系统管理员！");
                bool result = VisitorRecordService.DeleteVisitorRecord(SystemID, CompanyID);
                if (result)
                    return Success("成功！");
                else
                    return Error("失败！");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }



    }
}