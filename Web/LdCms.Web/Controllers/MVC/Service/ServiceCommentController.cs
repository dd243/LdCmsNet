﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LdCms.Web.Controllers.MVC.Service
{
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Service;
    using LdCms.Common.Extension;
    using LdCms.Web.Models;
    using LdCms.Web.Services;
    using LdCms.Common.Json;
    using LdCms.Model.Comment;

    /// <summary>
    /// 
    /// </summary>
    [AdminAuthorize(Roles = "Admins")]
    public class ServiceCommentController : BaseController
    {
        private readonly IBaseManager BaseManager;
        private readonly ICommentService CommentService;
        private readonly ICommentAppendService CommentAppendService;
        public ServiceCommentController(IBaseManager BaseManager, ICommentService CommentService, ICommentAppendService CommentAppendService) : base(BaseManager)
        {
            this.BaseManager = BaseManager;
            this.CommentService = CommentService;
            this.CommentAppendService = CommentAppendService;
        }

        public override ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.客服管理.评价管理.列表);
                if (!IsPermission(funcId))
                    return ToPermission(funcId);
                string startTime = GetQueryString("datemin");
                string endTime = GetQueryString("datemax");
                string status = GetQueryString("status");
                string keyword = GetQueryString("keyword");
                ViewBag.DateMin = startTime;
                ViewBag.DateMax = endTime;
                ViewBag.Status = status;
                ViewBag.Keyword = keyword;

                int total = 100;
                List<Ld_Service_Comment> lists = new List<Ld_Service_Comment>();
                string strKeyword = string.Format("{0}{1}{2}", startTime, status, keyword);
                if (string.IsNullOrWhiteSpace(strKeyword))
                    lists = CommentService.GetCommentTop(SystemID, CompanyID, total);
                else
                    lists = CommentService.SearchComment(SystemID, CompanyID, startTime, endTime, status, keyword, total);
                ViewBag.Count = CommentService.CountComment(SystemID, CompanyID);
                return View(lists);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }
        public ActionResult Show(string commentId)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.客服管理.评价管理.查看);
                if (!IsPermission(funcId))
                    return ToPermission(funcId);

                var entity = CommentService.GetComment(SystemID, CompanyID, commentId);
                var lists = CommentAppendService.GetCommentAppend(SystemID, CompanyID, commentId);
                var result = ToServiceComment(entity, lists);
                return View(result);
            }
            catch (Exception ex)
            {
                return ToError(ex.Message);
            }
        }



        [HttpPost]
        public JsonResult Delete(string commentId)
        {
            try
            {
                string funcId = PermissionEnum.CodeFormat((int)PermissionEnum.客服管理.评价管理.删除);
                if (!IsPermission(funcId)) { return Error("您没有操作权限，请联系系统管理员！"); }

                var result = CommentService.DeleteComment(SystemID, CompanyID, commentId);
                if (result)
                    return Success("ok");
                else
                    return Error("fail");
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        private ServiceComment ToServiceComment(Ld_Service_Comment entity, List<Ld_Service_CommentAppend> details)
        {
            try
            {
                return new ServiceComment()
                {
                    SystemID = entity.SystemID,
                    CompanyID = entity.CompanyID,
                    CommentID = entity.CommentID,
                    MemberID = entity.MemberID,
                    OrderID = entity.OrderID,
                    Images = entity.Images,
                    ServiceStatus = entity.ServiceStatus,
                    ServicePoints = entity.ServicePoints,
                    UpNum = entity.UpNum,
                    DownNum = entity.DownNum,
                    Description = entity.Description,
                    State = entity.State,
                    CreateDate = entity.CreateDate,
                    Details = details.ToJson().ToObject<List<ServiceCommentAppend>>()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}