﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Compilation;
using Autofac;
using Autofac.Integration.Mvc;

namespace LdCms.Web.Autofac
{
    using LdCms.EF.DbContext;
    using LdCms.EF.DbModels;
    using LdCms.Web.Services;
    using LdCms.Web.Services.IManager;
    using LdCms.Web.Services.Manager;
    /// <summary>
    /// 
    /// </summary>
    public class AutofacModule
    {
        /// <summary>
        /// 
        /// </summary>
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetCallingAssembly()).PropertiesAutowired();               // 调用当前方法的方法 所在的程序集
            //builder.RegisterControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();           // 当前方法所在程序集

            //1、无接口类注入
            //builder.RegisterType<BLL.newsClassBLL>().AsSelf().InstancePerRequest().PropertiesAutowired();
            //2、注册应用程序依赖项。
            //builder.RegisterType<DAL.Sys.CodeDAL>().As<IDAL.Sys.ICodeDAL>().InstancePerLifetimeScope();
            //3、 获取包含继承了IService接口类的程序集
            //var assemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>().Where(assembly => assembly.GetTypes().FirstOrDefault(type => type.GetInterfaces().Contains(typeof(IService))) != null);


            //注册应用程序依赖项。
            builder.RegisterType<UserServices>().As<IUserServices>().SingleInstance();                      //同一个Lifetime生成的对象是同一个实例
            builder.RegisterType<StaffServices>().As<IStaffServices>().InstancePerLifetimeScope();          //单例模式，每次调用，都会使用同一个实例化的对象；每次都用同一个对象
            builder.RegisterType<LogServices>().As<ILogServices>().InstancePerDependency();                 //默认模式，每次调用，都会重新实例化对象；每次请求都创建一个新的对象

            builder.Register(c => new BaseManager(c.Resolve<IBLL.Sys.IOperatorService>())).As<IBaseManager>().InstancePerLifetimeScope();


            #region 操作日志类注册容器 Manager
            //系统部分
            builder.Register(c => new TableOperationManager<Ld_Sys_AccessCorsHost>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_AccessCorsHost>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_Code>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_Code>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_Config>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_Config>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_Function>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_Function>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_InterfaceAccessToken>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_InterfaceAccessToken>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_InterfaceAccessWhiteList>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_InterfaceAccessWhiteList>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_InterfaceAccount>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_InterfaceAccount>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_Operator>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_Operator>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_OperatorRole>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_OperatorRole>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_Role>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_Role>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_RoleFunction>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_RoleFunction>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Sys_Version>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Sys_Version>>().InstancePerLifetimeScope();

            //公司部分
            builder.Register(c => new TableOperationManager<Ld_Institution_Company>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Company>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Institution_Dealer>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Dealer>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Institution_Department>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Department>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Institution_Position>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Position>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Institution_Staff>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Staff>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Institution_Store>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Store>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Institution_Supplier>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Supplier>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Institution_Warehouse>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Institution_Warehouse>>().InstancePerLifetimeScope();


            //日志部分
            builder.Register(c => new TableOperationManager<Ld_Log_ErrorRecord>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Log_ErrorRecord>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Log_LoginRecord>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Log_LoginRecord>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Log_Table>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Log_Table>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Log_TableDetails>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Log_TableDetails>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Log_TableOperation>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Log_TableOperation>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Log_VisitorRecord>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Log_VisitorRecord>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Log_WebApiAccessRecord>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Log_WebApiAccessRecord>>().InstancePerLifetimeScope();

            //会员部分
            builder.Register(c => new TableOperationManager<Ld_Member_Settings>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_Settings>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_Classify>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_Classify>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_Rank>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_Rank>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_Account>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_Account>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_AccountAccessToken>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_AccountAccessToken>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_AccountRefreshToken>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_AccountRefreshToken>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_Address>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_Address>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_AmountRecord>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_AmountRecord>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_Invoice>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_Invoice>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_LoginLogs>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_LoginLogs>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Member_PointRecord>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Member_PointRecord>>().InstancePerLifetimeScope();


            //客服部分
            builder.Register(c => new TableOperationManager<Ld_Service_MessageBoard>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Service_MessageBoard>>().InstancePerLifetimeScope();

            //基础公共部分
            builder.Register(c => new TableOperationManager<Ld_Basics_Media>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Basics_Media>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Basics_MediaInterface>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Basics_MediaInterface>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Basics_MediaMember>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Basics_MediaMember>>().InstancePerLifetimeScope();

            //资讯部分
            builder.Register(c => new TableOperationManager<Ld_Info_Artice>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Info_Artice>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Info_Block>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Info_Block>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Info_Class>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Info_Class>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Info_Notice>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Info_Notice>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Info_NoticeCategory>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Info_NoticeCategory>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Info_Page>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Info_Page>>().InstancePerLifetimeScope();

            //扩展部分
            builder.Register(c => new TableOperationManager<Ld_Extend_Advertisement>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Extend_Advertisement>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Extend_AdvertisementDetails>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Extend_AdvertisementDetails>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Extend_Link>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Extend_Link>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Extend_LinkGroup>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Extend_LinkGroup>>().InstancePerLifetimeScope();
            builder.Register(c => new TableOperationManager<Ld_Extend_SearchKeyword>(c.Resolve<IBLL.Log.ITableService>(), c.Resolve<IBLL.Log.ITableDetailsService>(), c.Resolve<IBLL.Log.ITableOperationService>())).As<ITableOperationManager<Ld_Extend_SearchKeyword>>().InstancePerLifetimeScope();



            #endregion


            #region DAL
            //Sys
            builder.Register(c => new DAL.Sys.AccessCorsHostDAL()).As<IDAL.Sys.IAccessCorsHostDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.CodeDAL()).As<IDAL.Sys.ICodeDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.ConfigDAL()).As<IDAL.Sys.IConfigDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.FunctionDAL()).As<IDAL.Sys.IFunctionDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.RoleDAL()).As<IDAL.Sys.IRoleDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.OperatorDAL()).As<IDAL.Sys.IOperatorDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.InterfaceAccountDAL()).As<IDAL.Sys.IInterfaceAccountDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.InterfaceAccessWhiteListDAL()).As<IDAL.Sys.IInterfaceAccessWhiteListDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Sys.InterfaceAccessTokenDAL()).As<IDAL.Sys.IInterfaceAccessTokenDAL>().InstancePerLifetimeScope();


            builder.Register(c => new DAL.Log.TableDAL()).As<IDAL.Log.ITableDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Log.TableDetailsDAL()).As<IDAL.Log.ITableDetailsDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Log.TableOperationDAL()).As<IDAL.Log.ITableOperationDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Log.ErrorRecordDAL()).As<IDAL.Log.IErrorRecordDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Log.LoginRecordDAL()).As<IDAL.Log.ILoginRecordDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Log.VisitorRecordDAL()).As<IDAL.Log.IVisitorRecordDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Log.WebApiAccessRecordDAL()).As<IDAL.Log.IWebApiAccessRecordDAL>().InstancePerLifetimeScope();

            builder.Register(c => new DAL.Institution.CompanyDAL()).As<IDAL.Institution.ICompanyDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Institution.DepartmentDAL()).As<IDAL.Institution.IDepartmentDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Institution.PositionDAL()).As<IDAL.Institution.IPositionDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Institution.StoreDAL()).As<IDAL.Institution.IStoreDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Institution.StaffDAL()).As<IDAL.Institution.IStaffDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Institution.StaffAccessTokenDAL()).As<IDAL.Institution.IStaffAccessTokenDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Institution.StaffRefreshTokenDAL()).As<IDAL.Institution.IStaffRefreshTokenDAL>().InstancePerLifetimeScope();


            builder.Register(c => new DAL.Member.SettingsDAL()).As<IDAL.Member.ISettingsDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.ClassifyDAL()).As<IDAL.Member.IClassifyDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.RankDAL()).As<IDAL.Member.IRankDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.AccountDAL()).As<IDAL.Member.IAccountDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.AccountAccessTokenDAL()).As<IDAL.Member.IAccountAccessTokenDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.LoginLogsDAL()).As<IDAL.Member.ILoginLogsDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.InvoiceDAL()).As<IDAL.Member.IInvoiceDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.AddressDAL()).As<IDAL.Member.IAddressDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Member.PointRecordDAL()).As<IDAL.Member.IPointRecordDAL>().InstancePerLifetimeScope();


            builder.Register(c => new DAL.Service.MessageBoardDAL()).As<IDAL.Service.IMessageBoardDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Service.CommentDAL()).As<IDAL.Service.ICommentDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Service.CommentAppendDAL()).As<IDAL.Service.ICommentAppendDAL>().InstancePerLifetimeScope();


            builder.Register(c => new DAL.Basics.ProvinceDAL()).As<IDAL.Basics.IProvinceDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Basics.CityDAL()).As<IDAL.Basics.ICityDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Basics.AreaDAL()).As<IDAL.Basics.IAreaDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Basics.MediaDAL()).As<IDAL.Basics.IMediaDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Basics.MediaInterfaceDAL()).As<IDAL.Basics.IMediaInterfaceDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Basics.MediaMemberDAL()).As<IDAL.Basics.IMediaMemberDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Basics.MediaStaffDAL()).As<IDAL.Basics.IMediaStaffDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Basics.VMediaDAL()).As<IDAL.Basics.IVMediaDAL>().InstancePerLifetimeScope();

            builder.Register(c => new DAL.Info.NoticeCategoryDAL()).As<IDAL.Info.INoticeCategoryDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Info.NoticeDAL()).As<IDAL.Info.INoticeDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Info.BlockDAL()).As<IDAL.Info.IBlockDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Info.ClassDAL()).As<IDAL.Info.IClassDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Info.PageDAL()).As<IDAL.Info.IPageDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Info.ArticeDAL()).As<IDAL.Info.IArticeDAL>().InstancePerLifetimeScope();

            builder.Register(c => new DAL.Extend.SearchKeywordDAL()).As<IDAL.Extend.ISearchKeywordDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Extend.AdvertisementDAL()).As<IDAL.Extend.IAdvertisementDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Extend.AdvertisementDetailsDAL()).As<IDAL.Extend.IAdvertisementDetailsDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Extend.LinkDAL()).As<IDAL.Extend.ILinkDAL>().InstancePerLifetimeScope();
            builder.Register(c => new DAL.Extend.LinkGroupDAL()).As<IDAL.Extend.ILinkGroupDAL>().InstancePerLifetimeScope();


            #endregion


            #region BLL
            //Sys
            builder.Register(c => new BLL.Sys.AccessCorsHostService(c.Resolve<IDAL.Sys.IAccessCorsHostDAL>())).As<IBLL.Sys.IAccessCorsHostService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.CodeService(c.Resolve<IDAL.Sys.ICodeDAL>())).As<IBLL.Sys.ICodeService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.ConfigService(c.Resolve<IDAL.Sys.IConfigDAL>())).As<IBLL.Sys.IConfigService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.FunctionService(c.Resolve<IDAL.Sys.IFunctionDAL>())).As<IBLL.Sys.IFunctionService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.RoleService(c.Resolve<IDAL.Sys.IRoleDAL>())).As<IBLL.Sys.IRoleService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.OperatorService(c.Resolve<IDAL.Sys.IOperatorDAL>())).As<IBLL.Sys.IOperatorService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.InterfaceAccountService(c.Resolve<IDAL.Sys.IInterfaceAccountDAL>())).As<IBLL.Sys.IInterfaceAccountService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.InterfaceAccessWhiteListService(c.Resolve<IDAL.Sys.IInterfaceAccessWhiteListDAL>())).As<IBLL.Sys.IInterfaceAccessWhiteListService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Sys.InterfaceAccessTokenService(c.Resolve<IDAL.Sys.IInterfaceAccessTokenDAL>())).As<IBLL.Sys.IInterfaceAccessTokenService>().InstancePerLifetimeScope();


            builder.Register(c => new BLL.Log.TableService(c.Resolve<IDAL.Log.ITableDAL>())).As<IBLL.Log.ITableService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Log.TableDetailsService(c.Resolve<IDAL.Log.ITableDetailsDAL>())).As<IBLL.Log.ITableDetailsService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Log.TableOperationService(c.Resolve<IDAL.Log.ITableOperationDAL>())).As<IBLL.Log.ITableOperationService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Log.ErrorRecordService(c.Resolve<IDAL.Log.IErrorRecordDAL>())).As<IBLL.Log.IErrorRecordService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Log.LoginRecordService(c.Resolve<IDAL.Log.ILoginRecordDAL>())).As<IBLL.Log.ILoginRecordService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Log.VisitorRecordService(c.Resolve<IDAL.Log.IVisitorRecordDAL>())).As<IBLL.Log.IVisitorRecordService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Log.WebApiAccessRecordService(c.Resolve<IDAL.Log.IWebApiAccessRecordDAL>())).As<IBLL.Log.IWebApiAccessRecordService>().InstancePerLifetimeScope();

            builder.Register(c => new BLL.Institution.CompanyService(c.Resolve<IDAL.Institution.ICompanyDAL>())).As<IBLL.Institution.ICompanyService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Institution.DepartmentService(c.Resolve<IDAL.Institution.IDepartmentDAL>())).As<IBLL.Institution.IDepartmentService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Institution.PositionService(c.Resolve<IDAL.Institution.IPositionDAL>())).As<IBLL.Institution.IPositionService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Institution.StoreService(c.Resolve<IDAL.Institution.IStoreDAL>())).As<IBLL.Institution.IStoreService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Institution.StaffService(c.Resolve<IDAL.Institution.IStaffDAL>())).As<IBLL.Institution.IStaffService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Institution.StaffAccessTokenService(c.Resolve<IDAL.Institution.IStaffAccessTokenDAL>())).As<IBLL.Institution.IStaffAccessTokenService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Institution.StaffRefreshTokenService(c.Resolve<IDAL.Institution.IStaffRefreshTokenDAL>())).As<IBLL.Institution.IStaffRefreshTokenService>().InstancePerLifetimeScope();


            builder.Register(c => new BLL.Member.SettingsService(c.Resolve<IDAL.Member.ISettingsDAL>())).As<IBLL.Member.ISettingsService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.ClassifyService(c.Resolve<IDAL.Member.IClassifyDAL>())).As<IBLL.Member.IClassifyService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.RankService(c.Resolve<IDAL.Member.IRankDAL>())).As<IBLL.Member.IRankService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.AccountService(c.Resolve<IDAL.Member.IAccountDAL>())).As<IBLL.Member.IAccountService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.AccountAccessTokenService(c.Resolve<IDAL.Member.IAccountAccessTokenDAL>())).As<IBLL.Member.IAccountAccessTokenService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.LoginLogsService(c.Resolve<IDAL.Member.ILoginLogsDAL>())).As<IBLL.Member.ILoginLogsService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.InvoiceService(c.Resolve<IDAL.Member.IInvoiceDAL>())).As<IBLL.Member.IInvoiceService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.AddressService(c.Resolve<IDAL.Member.IAddressDAL>())).As<IBLL.Member.IAddressService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Member.PointRecordService(c.Resolve<IDAL.Member.IPointRecordDAL>())).As<IBLL.Member.IPointRecordService>().InstancePerLifetimeScope();


            builder.Register(c => new BLL.Service.MessageBoardService(c.Resolve<IDAL.Service.IMessageBoardDAL>())).As<IBLL.Service.IMessageBoardService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Service.CommentService(c.Resolve<IDAL.Service.ICommentDAL>())).As<IBLL.Service.ICommentService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Service.CommentAppendService(c.Resolve<IDAL.Service.ICommentAppendDAL>())).As<IBLL.Service.ICommentAppendService>().InstancePerLifetimeScope();


            builder.Register(c => new BLL.Basics.ProvinceService(c.Resolve<IDAL.Basics.IProvinceDAL>())).As<IBLL.Basics.IProvinceService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Basics.CityService(c.Resolve<IDAL.Basics.ICityDAL>())).As<IBLL.Basics.ICityService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Basics.AreaService(c.Resolve<IDAL.Basics.IAreaDAL>())).As<IBLL.Basics.IAreaService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Basics.MediaService(c.Resolve<IDAL.Basics.IMediaDAL>())).As<IBLL.Basics.IMediaService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Basics.MediaInterfaceService(c.Resolve<IDAL.Basics.IMediaInterfaceDAL>())).As<IBLL.Basics.IMediaInterfaceService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Basics.MediaMemberService(c.Resolve<IDAL.Basics.IMediaMemberDAL>())).As<IBLL.Basics.IMediaMemberService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Basics.MediaStaffService(c.Resolve<IDAL.Basics.IMediaStaffDAL>())).As<IBLL.Basics.IMediaStaffService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Basics.VMediaService(c.Resolve<IDAL.Basics.IVMediaDAL>())).As<IBLL.Basics.IVMediaService>().InstancePerLifetimeScope();

            builder.Register(c => new BLL.Info.NoticeCategoryService(c.Resolve<IDAL.Info.INoticeCategoryDAL>())).As<IBLL.Info.INoticeCategoryService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Info.NoticeService(c.Resolve<IDAL.Info.INoticeDAL>())).As<IBLL.Info.INoticeService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Info.BlockService(c.Resolve<IDAL.Info.IBlockDAL>())).As<IBLL.Info.IBlockService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Info.ClassService(c.Resolve<IDAL.Info.IClassDAL>())).As<IBLL.Info.IClassService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Info.PageService(c.Resolve<IDAL.Info.IPageDAL>())).As<IBLL.Info.IPageService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Info.ArticeService(c.Resolve<IDAL.Info.IArticeDAL>())).As<IBLL.Info.IArticeService>().InstancePerLifetimeScope();

            builder.Register(c => new BLL.Extend.SearchKeywordService(c.Resolve<IDAL.Extend.ISearchKeywordDAL>())).As<IBLL.Extend.ISearchKeywordService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Extend.AdvertisementService(c.Resolve<IDAL.Extend.IAdvertisementDAL>())).As<IBLL.Extend.IAdvertisementService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Extend.AdvertisementDetailsService(c.Resolve<IDAL.Extend.IAdvertisementDetailsDAL>())).As<IBLL.Extend.IAdvertisementDetailsService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Extend.LinkService(c.Resolve<IDAL.Extend.ILinkDAL>())).As<IBLL.Extend.ILinkService>().InstancePerLifetimeScope();
            builder.Register(c => new BLL.Extend.LinkGroupService(c.Resolve<IDAL.Extend.ILinkGroupDAL>())).As<IBLL.Extend.ILinkGroupService>().InstancePerLifetimeScope();


            #endregion




            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}