﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LdCms.Web.Services
{
    using LdCms.IBLL.Sys;
    using LdCms.Common.Extension;
    using LdCms.Common.Utility;
    using System.Web.Mvc;


    /// <summary>
    /// 
    /// </summary>
    public partial class BaseManager: IBaseManager
    {
        private readonly IOperatorService OperatorService;
        public BaseManager(IOperatorService OperatorService)
        {
            this.OperatorService = OperatorService;
        }
        public int SystemID = BaseSystemConfig.SystemID;
        public bool IsPermission(string companyId, string staffId, string functionId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(functionId))
                    return false;
                bool result = OperatorService.VerifyOperatorPermissionPro(SystemID, companyId, staffId, functionId);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region 私有化共用方法
        public string GetQueryString(string name)
        {
            try
            {
                var result = HttpContext.Current.Request.GetQueryString(name);
                if (string.IsNullOrEmpty(result))
                    return "";
                else
                    return Utility.FilterText(result.ToString().Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string GetFormValue(string name)
        {
            try
            {
                var result = HttpContext.Current.Request.GetFormValue(name);
                if (string.IsNullOrEmpty(result))
                    return "";
                else
                    return Utility.FilterText(result.ToString().Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string GetFormValueArr(string name)
        {
            try
            {
                var result = HttpContext.Current.Request.GetFormValueArr(name);
                if (string.IsNullOrEmpty(result))
                    return "";
                else
                    return Utility.FilterText(result.ToString().Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string GetFormValue(FormCollection formValue, string name)
        {
            try
            {
                var result = formValue[name];
                if (string.IsNullOrEmpty(result))
                    return "";
                else
                    return Utility.FilterText(result.ToString().Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


    }
}
