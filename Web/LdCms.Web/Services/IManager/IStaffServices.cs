﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LdCms.Web.Services.IManager
{
    public interface IStaffServices
    {
        string GetStaffName();
    }
}
