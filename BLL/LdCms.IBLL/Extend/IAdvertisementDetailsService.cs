﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Extend
{
    using LdCms.EF.DbModels;
    public partial interface IAdvertisementDetailsService : IBaseService<Ld_Extend_AdvertisementDetails>
    {
        bool SaveAdvertisementDetails(Ld_Extend_AdvertisementDetails entity);
        bool UpdateAdvertisementDetails(Ld_Extend_AdvertisementDetails entity);
        bool DeleteAdvertisementDetails(int systemId, string companyId, string detailsId);
        bool DeleteAdvertisementDetailsByAdvertisementId(int systemId, string companyId, string advertisementId);
        Ld_Extend_AdvertisementDetails GetAdvertisementDetails(int systemId, string companyId, string detailsId);
        List<Ld_Extend_AdvertisementDetails> GetAdvertisementDetailsByAdvertisementId(int systemId, string companyId, string advertisementId);

    }
}
