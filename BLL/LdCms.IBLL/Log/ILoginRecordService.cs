﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Log
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    /// <summary>
    /// 
    /// </summary>
    public partial interface ILoginRecordService:IBaseService<Ld_Log_LoginRecord>
    {
        bool SaveLoginRecord(Ld_Log_LoginRecord entity);
        bool DeleteLoginRecord(long id);
        bool DeleteLoginRecord(int systemId);
        bool DeleteLoginRecord(int systemId, string companyId);

        Ld_Log_LoginRecord GetLoginRecord(long id);

        List<Ld_Log_LoginRecord> GetLoginRecordTop(int systemId, int count);
        List<Ld_Log_LoginRecord> GetLoginRecordTop(int systemId, string companyId, int count);
        List<Ld_Log_LoginRecord> GetLoginRecordPaging(int systemId, int pageId, int pageSize);
        List<Ld_Log_LoginRecord> GetLoginRecordPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Log_LoginRecord> SearchLoginRecord(int systemId, string startTime, string endTime, string clientId, string keyword, int count);
        List<Ld_Log_LoginRecord> SearchLoginRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword, int count);
        int CountLoginRecord(int systemId);
        int CountLoginRecord(int systemId, string companyId);
        int CountLoginRecord(int systemId, string startTime, string endTime, string clientId, string keyword);
        int CountLoginRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword);

    }
}
