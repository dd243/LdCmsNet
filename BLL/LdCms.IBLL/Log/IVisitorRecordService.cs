﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Log
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    /// <summary>
    /// 
    /// </summary>
    public partial interface IVisitorRecordService:IBaseService<Ld_Log_VisitorRecord>
    {
        bool SaveVisitorRecord(Ld_Log_VisitorRecord entity);
        bool DeleteVisitorRecord(long id);
        bool DeleteVisitorRecord(int systemId);
        bool DeleteVisitorRecord(int systemId, string companyId);

        Ld_Log_VisitorRecord GetVisitorRecord(long id);

        List<Ld_Log_VisitorRecord> GetVisitorRecordTop(int systemId, int count);
        List<Ld_Log_VisitorRecord> GetVisitorRecordTop(int systemId, string companyId, int count);
        List<Ld_Log_VisitorRecord> GetVisitorRecordPaging(int systemId, int pageId, int pageSize);
        List<Ld_Log_VisitorRecord> GetVisitorRecordPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Log_VisitorRecord> SearchVisitorRecord(int systemId, string startTime, string endTime, string clientId, string keyword, int count);
        List<Ld_Log_VisitorRecord> SearchVisitorRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword, int count);

        int CountVisitorRecord(int systemId);
        int CountVisitorRecord(int systemId, string companyId);
        int CountVisitorRecord(int systemId, string startTime, string endTime, string clientId, string keyword);
        int CountVisitorRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword);


    }
}
