﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Log
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    /// <summary>
    /// 
    /// </summary>
    public partial interface IWebApiAccessRecordService:IBaseService<Ld_Log_WebApiAccessRecord>
    {
        bool SaveWebApiAccessRecord(Ld_Log_WebApiAccessRecord entity);
        bool UpdateWebApiAccessRecordState(long id, string result, bool state);

        bool DeleteWebApiAccessRecord(long id);
        bool DeleteWebApiAccessRecord(int systemId);
        bool DeleteWebApiAccessRecord(int systemId, string companyId);

        Ld_Log_WebApiAccessRecord GetWebApiAccessRecord(long id);

        List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordTop(int systemId, int count);
        List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordTop(int systemId, string companyId, int count);
        List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordPaging(int systemId, int pageId, int pageSize);
        List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Log_WebApiAccessRecord> SearchWebApiAccessRecord(int systemId, string startTime, string endTime, string classId, string state, string keyword, int count);
        List<Ld_Log_WebApiAccessRecord> SearchWebApiAccessRecord(int systemId, string companyId, string startTime, string endTime, string classId, string state, string keyword, int count);

        int CountWebApiAccessRecord(int systemId);
        int CountWebApiAccessRecord(int systemId, string companyId);
        int CountWebApiAccessRecord(int systemId, string startTime, string endTime, string classId, string state, string keyword);
        int CountWebApiAccessRecord(int systemId, string companyId, string startTime, string endTime, string classId, string state, string keyword);




    }
}
