﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Basics
{
    using LdCms.EF.DbModels;
    public partial interface ICityService:IBaseService<Ld_Basics_City>
    {
        List<Ld_Basics_City> GetCity(int systemId, int provinceId);
    }
}
