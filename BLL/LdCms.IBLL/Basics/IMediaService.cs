﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Basics
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    public partial interface IMediaService:IBaseService<Ld_Basics_Media>
    {
        bool SaveMedia(Ld_Basics_Media entity);
        int SaveMediaInterface(string appId, Ld_Basics_Media entity);
        int SaveMediaInterface(string appId, List<Ld_Basics_Media> lists);
        int SaveMediaMember(string memberId, Ld_Basics_Media entity);
        int SaveMediaMember(string memberId, List<Ld_Basics_Media> lists);
        int SaveMediaStaff(string staffId, Ld_Basics_Media entity);
        int SaveMediaStaff(string staffId, List<Ld_Basics_Media> lists);
        Ld_Basics_Media GetMedia(int systemId, string companyId, string mediaId);

    }
}
