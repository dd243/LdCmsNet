﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    /// <summary>
    /// 
    /// </summary>
    public partial interface IPointRecordService:IBaseService<Ld_Member_PointRecord>
    {
        bool SavePointRecord(Ld_Member_PointRecord entity);
        Ld_Member_PointRecord GetPointRecord(int systemId, int id);
        List<Ld_Member_PointRecord> GetPointRecordTop(int systemId, string companyId, int count);
        List<Ld_Member_PointRecord> GetPointRecordPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Member_PointRecord> SearchPointRecord(int systemId, string companyId, string startTime, string endTime, string keyword, int count);
        List<Ld_Member_PointRecord> GetPointRecordPagingByMemberId(int systemId, string companyId, string memberId, int pageId, int pageSize);
        int CountPointRecord(int systemId, string companyId);
        int CountPointRecord(int systemId, string companyId, string memberId);
        int CountPointRecord(int systemId, string companyId, string startTime, string endTime, string keyword);


        bool AddPointRecordPro(int systemId, string companyId, string memberId, int classId, string className, int typeId, string typeName, int points, string tasksId, string body, string account, string nickname, string ipAddress, string remark, bool state);

    }
}
