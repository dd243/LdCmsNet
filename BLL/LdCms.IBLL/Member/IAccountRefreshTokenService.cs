﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Member
{
    using EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    public partial interface IAccountRefreshTokenService:IBaseService<Ld_Member_AccountRefreshToken>
    {
    }
}
