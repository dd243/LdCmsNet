﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Institution
{
    using LdCms.EF.DbModels;
    public partial interface IStaffRefreshTokenService:IBaseService<Ld_Institution_StaffRefreshToken>
    {
    }
}
