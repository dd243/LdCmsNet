﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Service
{
    using LdCms.EF.DbModels;
    public partial interface ICommentService:IBaseService<Ld_Service_Comment>
    {
        bool SaveComment(Ld_Service_Comment entity);
        bool DeleteComment(int systemId, string companyId, string commentId);
        Ld_Service_Comment GetComment(int systemId, string companyId, string commentId);
        Ld_Service_Comment GetCommentByOrderId(int systemId, string companyId, string orderId);
        List<Ld_Service_Comment> GetCommentTop(int systemId, string companyId, int count);
        List<Ld_Service_Comment> GetCommentPaging(int systemId, string companyId, int pageId, int pageSize);
        List<Ld_Service_Comment> SearchComment(int systemId, string companyId, string startTime, string endTime, string status, string keyword, int count);
        List<Ld_Service_Comment> GetCommentPagingByMemberId(int systemId, string companyId, string memberId, int pageId, int pageSize);
        int CountComment(int systemId, string companyId);
        int CountComment(int systemId, string companyId, string memberId);
        int CountComment(int systemId, string companyId, string startTime, string endTime, string status, string keyword);


    }
}
