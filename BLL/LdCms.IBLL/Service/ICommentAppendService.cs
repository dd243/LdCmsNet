﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IBLL.Service
{
    using LdCms.EF.DbModels;
    public partial interface ICommentAppendService:IBaseService<Ld_Service_CommentAppend>
    {
        bool SaveCommentAppend(Ld_Service_CommentAppend entity);
        Ld_Service_CommentAppend GetCommentAppend(int systemId, string companyId, string commentId, string appendId);
        List<Ld_Service_CommentAppend> GetCommentAppend(int systemId, string companyId, string commentId);

    }
}
