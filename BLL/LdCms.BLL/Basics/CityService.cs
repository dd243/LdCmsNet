﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Basics
{
    using LdCms.EF.DbContext;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Basics;
    using LdCms.IDAL.Basics;
    using LdCms.Common.Json;
    public partial class CityService:BaseService<Ld_Basics_City>,ICityService
    {
        private readonly ICityDAL CityDAL;
        public CityService(ICityDAL CityDAL)
        {
            this.CityDAL = CityDAL;
            this.Dal = CityDAL;
        }
        public override void SetDal()
        {
            Dal = CityDAL;
        }

        public List<Ld_Basics_City> GetCity(int systemId,int provinceId)
        {
            try
            {
                return FindList(m => m.SystemID == systemId && m.ProvinceID == provinceId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
