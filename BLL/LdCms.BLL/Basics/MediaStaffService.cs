﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Basics
{
    using LdCms.EF.DbContext;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Basics;
    using LdCms.IDAL.Basics;
    using LdCms.Common.Json;

    public partial class MediaStaffService:BaseService<Ld_Basics_MediaStaff>,IMediaStaffService
    {
        private readonly IMediaStaffDAL MediaStaffDAL;
        public MediaStaffService(IMediaStaffDAL MediaStaffDAL)
        {
            this.MediaStaffDAL = MediaStaffDAL;
            this.Dal = MediaStaffDAL;
        }
        public override void SetDal()
        {
            Dal = MediaStaffDAL;
        }

    }
}
