﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Member
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Member;
    using LdCms.IDAL.Member;
    using LdCms.Common.Json;
    using LdCms.Common.Extension;
    public partial class SettingsService:BaseService<Ld_Member_Settings>,ISettingsService
    {
        private readonly ISettingsDAL SettingsDAL;
        
        public SettingsService(ISettingsDAL SettingsDAL)
        {
            
            this.SettingsDAL = SettingsDAL;
            this.Dal = SettingsDAL;
        }
        public override void SetDal()
        {
            Dal = SettingsDAL;
        }
        public bool CreateSettings(int systemId, string companyId)
        {
            try
            {
                Ld_Member_Settings entity = new Ld_Member_Settings()
                {
                    SystemID=systemId,
                    CompanyID=companyId,
                    IsRegister=true,
                    IsSmsCode=false,
                    IsPoint=false,
                    NewUserPoint=0,
                    LoginPoint=0,
                    SignINPoint=0,
                    OrderPointType=1,
                    OrderPoint=0,
                    CommentPoint=0,
                    Account="admin",
                    NickName="创始者",
                    CreateDate=DateTime.Now
                };
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateSettings(Ld_Member_Settings entity)
        {
            try
            {
                entity.IsRegister = entity.IsRegister.ToBool();
                entity.IsSmsCode = entity.IsSmsCode.ToBool();
                entity.CreateDate = DateTime.Now;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Member_Settings GetSettings(int systemId, string companyId)
        {
            try
            {
                return Find(m => m.SystemID == systemId && m.CompanyID == companyId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
