﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Log
{
    using LdCms.EF.DbStoredProcedure;
    using LdCms.EF.DbModels;
    using LdCms.IBLL.Log;
    using LdCms.IDAL.Log;
    using LdCms.Common.Json;
    using LdCms.Common.Extension;
    using LdCms.Common.Utility;
    /// <summary>
    /// 
    /// </summary>
    public partial class LoginRecordService:BaseService<Ld_Log_LoginRecord>,ILoginRecordService
    {
        private readonly ILoginRecordDAL LoginRecordDAL;
        public LoginRecordService(ILoginRecordDAL LoginRecordDAL)
        {
            this.LoginRecordDAL = LoginRecordDAL;
            this.Dal = LoginRecordDAL;
        }
        public override void SetDal()
        {
            Dal = LoginRecordDAL;
        }

        public bool SaveLoginRecord(Ld_Log_LoginRecord entity)
        {
            try
            {
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteLoginRecord(long id)
        {
            try
            {
                return Delete(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteLoginRecord(int systemId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteLoginRecord(int systemId, string companyId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Ld_Log_LoginRecord GetLoginRecord(long id)
        {
            try
            {
                return Find(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Ld_Log_LoginRecord> GetLoginRecordTop(int systemId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                return FindListTop(expression, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_LoginRecord> GetLoginRecordTop(int systemId, string companyId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                return FindListTop(expression, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_LoginRecord> GetLoginRecordPaging(int systemId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.ID, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_LoginRecord> GetLoginRecordPaging(int systemId, string companyId, int pageId, int pageSize)
        {
            try
            {
                
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.ID, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_LoginRecord> SearchLoginRecord(int systemId, string startTime, string endTime, string clientId, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                byte btClientId = clientId.ToByte();
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == btClientId)
                && (m.Account.Contains(keyword) || m.NickName.Contains(keyword) || m.IpAddress.Contains(keyword)));
                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_LoginRecord> SearchLoginRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                byte btClientId = clientId.ToByte();
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == btClientId)
                && (m.Account.Contains(keyword) || m.NickName.Contains(keyword) || m.IpAddress.Contains(keyword)));
                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountLoginRecord(int systemId)
        {
            try
            {
                return Count(m => m.SystemID == systemId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountLoginRecord(int systemId, string companyId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountLoginRecord(int systemId, string startTime, string endTime, string clientId, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                byte btClientId = clientId.ToByte();
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == btClientId)
                && (m.Account.Contains(keyword) || m.NickName.Contains(keyword) || m.IpAddress.Contains(keyword)));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountLoginRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                byte btClientId = clientId.ToByte();
                var expression = ExtLinq.True<Ld_Log_LoginRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == btClientId)
                && (m.Account.Contains(keyword) || m.NickName.Contains(keyword) || m.IpAddress.Contains(keyword)));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
