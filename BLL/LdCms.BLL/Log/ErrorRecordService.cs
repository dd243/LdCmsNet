﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Log
{
    using LdCms.Common.Json;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Log;
    using LdCms.IDAL.Log;
    using LdCms.Common.Extension;
    using LdCms.Common.Utility;

    /// <summary>
    /// 
    /// </summary>
    public partial class ErrorRecordService:BaseService<Ld_Log_ErrorRecord>,IErrorRecordService
    {
        private readonly IErrorRecordDAL ErrorRecordDAL;
        public ErrorRecordService(IErrorRecordDAL ErrorRecordDAL)
        {
            this.ErrorRecordDAL = ErrorRecordDAL;
            this.Dal = ErrorRecordDAL;
        }
        public override void SetDal()
        {
            Dal = ErrorRecordDAL;
        }

        public bool SaveErrorRecord(Ld_Log_ErrorRecord entity)
        {
            try
            {
                entity.ClientID = entity.ClientID.ToInt().ToByte();
                entity.State = entity.State.ToBool();
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteErrorRecord(long id)
        {
            try
            {
                return Delete(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteErrorRecord(int systemId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Log_ErrorRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Ld_Log_ErrorRecord GetErrorRecord(long id)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_ErrorRecord>();
                expression = expression.And(m => m.ID == id);
                return Find(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Ld_Log_ErrorRecord> GetErrorRecordTop(int systemId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Log_ErrorRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_ErrorRecord> GetErrorRecordPaging(int systemId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_ErrorRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_ErrorRecord> SearchErrorRecord(int systemId, string startTime, string endTime, string clientId, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToInt();
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Log_ErrorRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClientId)
                && m.Url.Contains(keyword));
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountErrorRecord(int systemId)
        {
            try
            {
                return Count(m => m.SystemID == systemId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountErrorRecord(int systemId, string startTime, string endTime, string clientId, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToInt();
                var expression = ExtLinq.True<Ld_Log_ErrorRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClientId)
                && m.Url.Contains(keyword));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
