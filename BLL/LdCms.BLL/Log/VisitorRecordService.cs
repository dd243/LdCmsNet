﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Log
{
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    using LdCms.Common.Utility;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Log;
    using LdCms.IDAL.Log;
    /// <summary>
    /// 
    /// </summary>
    public partial class VisitorRecordService:BaseService<Ld_Log_VisitorRecord>,IVisitorRecordService
    {
        private readonly IVisitorRecordDAL VisitorRecordDAL;
        public VisitorRecordService(IVisitorRecordDAL VisitorRecordDAL)
        {
            this.VisitorRecordDAL = VisitorRecordDAL;
            this.Dal = VisitorRecordDAL;
        }
        public override void SetDal()
        {
            Dal = VisitorRecordDAL;
        }

        public bool SaveVisitorRecord(Ld_Log_VisitorRecord entity)
        {
            try
            {
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteVisitorRecord(long id)
        {
            try
            {
                return Delete(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteVisitorRecord(int systemId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteVisitorRecord(int systemId,string companyId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Ld_Log_VisitorRecord GetVisitorRecord(long id)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.ID == id);
                return Find(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Ld_Log_VisitorRecord> GetVisitorRecordTop(int systemId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_VisitorRecord> GetVisitorRecordTop(int systemId, string companyId, int count)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_VisitorRecord> GetVisitorRecordPaging(int systemId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_VisitorRecord> GetVisitorRecordPaging(int systemId, string companyId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                return FindListPaging(expression, m => m.CreateDate, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_VisitorRecord> SearchVisitorRecord(int systemId, string startTime, string endTime, string clientId, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToInt();
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClientId)
                && (m.AbsoluteUri.Contains(keyword) || m.IpAddress.Contains(keyword)));
                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_VisitorRecord> SearchVisitorRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToInt();
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClientId)
                && (m.AbsoluteUri.Contains(keyword) || m.IpAddress.Contains(keyword)));
                int total = Utility.ToTopTotal(count);
                return FindListTop(expression, m => m.CreateDate, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountVisitorRecord(int systemId)
        {
            try
            {
                return Count(m => m.SystemID == systemId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountVisitorRecord(int systemId,string companyId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountVisitorRecord(int systemId, string startTime, string endTime, string clientId, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToInt();
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClientId)
                && (m.AbsoluteUri.Contains(keyword) || m.IpAddress.Contains(keyword)));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountVisitorRecord(int systemId, string companyId, string startTime, string endTime, string clientId, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                int intClientId = clientId.ToInt();
                var expression = ExtLinq.True<Ld_Log_VisitorRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID==companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(clientId) ? true : m.ClientID.Value == intClientId)
                && (m.AbsoluteUri.Contains(keyword) || m.IpAddress.Contains(keyword)));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }





    }
}
