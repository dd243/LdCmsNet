﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Log
{
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Log;
    using LdCms.IDAL.Log;
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    /// <summary>
    /// 
    /// </summary>
    public partial class TableService:BaseService<Ld_Log_Table>,ITableService
    {
        private readonly ITableDAL TableDAL;
        public TableService(ITableDAL TableDAL)
        {
            this.TableDAL = TableDAL;
            this.Dal = TableDAL;
        }
        public override void SetDal()
        {
            Dal = TableDAL;
        }

        public bool IsTableByName(string tableName)
        {
            return IsExists(m => m.TableName == tableName);
        }
        public bool SaveTable(Ld_Log_Table entity)
        {
            try
            {
                string tableName = entity.TableName;
                if (IsTableByName(tableName))
                    throw new Exception("表名已存在！");
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SaveTable(Ld_Log_Table entity, List<Ld_Log_TableDetails> list)
        {
            try
            {
                string tableName = entity.TableName;
                if (IsTableByName(tableName))
                    throw new Exception("表名已存在！");

                int intnum = 0;
                var dbContext = new DAL.BaseDAL();
                var db = dbContext.DbEntities();
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        entity.CreateDate = DateTime.Now;
                        dbContext.Add(entity);
                        foreach (var m in list)
                        {
                            m.CreateDate = DateTime.Now;
                            dbContext.Add(m);
                        }
                        intnum = db.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                    }
                    return intnum;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateTableBusinessName(string tableId, string businessName, string remark)
        {
            try
            {
                var entity = GetTable(tableId);
                if (entity == null)
                    throw new Exception("table id invalid！");
                entity.BusinessName = businessName;
                entity.Remark = remark;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteTable()
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_Table>();
                expression = expression.And(m => true);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteTable(string tableId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_Table>();
                expression = expression.And(m => m.TableID == tableId);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteTable(Ld_Log_Table entity)
        {
            try
            {
                return Delete(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Ld_Log_Table GetTable(string tableId)
        {
            try
            {
                return Find(m => m.TableID == tableId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Log_Table GetTableByName(string tableName)
        {
            try
            {
                return Find(m => m.TableName == tableName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_Table> GetTableTop(int count)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_Table>();
                expression = expression.And(m => true);
                return FindListTop(expression, m => m.TableID, false, count).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_Table> GetTablePaging(int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_Table>();
                expression = expression.And(m => true);
                return FindListPaging(expression, m => m.TableName, true, pageId, pageSize).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_Table> SearchTable(string keyword)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_Table>();
                expression = expression.And(m => m.TableName.Contains(keyword));
                return FindList(expression, m => m.TableName, true).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountTable()
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_Table>();
                expression = expression.And(m => true);
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountTable(string keyword)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_Table>();
                expression = expression.And(m => m.TableName.Contains(keyword));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
