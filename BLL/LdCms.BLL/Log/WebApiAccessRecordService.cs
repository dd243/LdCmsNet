﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Log
{
    using LdCms.Common.Extension;
    using LdCms.Common.Json;
    using LdCms.Common.Utility;
    using LdCms.EF.DbModels;
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Log;
    using LdCms.IDAL.Log;
    

    /// <summary>
    /// 
    /// </summary>
    public partial class WebApiAccessRecordService:BaseService<Ld_Log_WebApiAccessRecord>,IWebApiAccessRecordService
    {
        private readonly IWebApiAccessRecordDAL SysWebApiAccessRecordDAL;
        public WebApiAccessRecordService(IWebApiAccessRecordDAL SysWebApiAccessRecordDAL)
        {
            this.SysWebApiAccessRecordDAL = SysWebApiAccessRecordDAL;
            this.Dal = SysWebApiAccessRecordDAL;
        }
        public override void SetDal()
        {
            Dal = SysWebApiAccessRecordDAL;
        }

        public bool IsExists(long id)
        {
            try
            {
                return IsExists(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool SaveWebApiAccessRecord(Ld_Log_WebApiAccessRecord entity)
        {
            try
            {
                entity.State = false;
                entity.CreateDate = DateTime.Now;
                entity.UpdateDate = null;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool UpdateWebApiAccessRecordState(long id, string result, bool state)
        {
            try
            {
                var entity = GetWebApiAccessRecord(id);
                entity.Result = result;
                entity.State = state;
                entity.UpdateDate = DateTime.Now;
                return Update(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool DeleteWebApiAccessRecord(long id)
        {
            try
            {
                return Delete(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteWebApiAccessRecord(int systemId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool DeleteWebApiAccessRecord(int systemId,string companyId)
        {
            try
            {
                DateTime time = DateTime.Now.AddDays(-3);
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.CreateDate.Value.Date <= time.Date);
                return Delete(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Ld_Log_WebApiAccessRecord GetWebApiAccessRecord(long id)
        {
            try
            {
                if (!IsExists(id))
                    throw new Exception("id invalid！");
                return Find(m => m.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordTop(int systemId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                var expressionScalarLambda = GetExpressionScalarLambda();
                return FindListTop(expression, expressionScalarLambda, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordTop(int systemId,string companyId, int count)
        {
            try
            {
                int total = Utility.ToTopTotal(count);
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                var expressionScalarLambda = GetExpressionScalarLambda();
                return FindListTop(expression, expressionScalarLambda, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordPaging(int systemId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                var expressionScalarLambda = GetExpressionScalarLambda();
                return FindListPaging(expression, expressionScalarLambda, m => m.ID, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_WebApiAccessRecord> GetWebApiAccessRecordPaging(int systemId, string companyId, int pageId, int pageSize)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId);
                int pageIndex = Utility.ToPageIndex(pageId);
                int pageCount = Utility.ToPageCount(pageSize);
                var expressionScalarLambda = GetExpressionScalarLambda();
                return FindListPaging(expression, expressionScalarLambda, m => m.ID, false, pageIndex, pageCount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_WebApiAccessRecord> SearchWebApiAccessRecord(int systemId, string startTime, string endTime, string classId, string state, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                bool blnState = state.ToBool();
                byte btClassId = classId.ToByte();
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(classId) ? true : m.ClassID.Value == btClassId)
                && (string.IsNullOrEmpty(state) ? true : m.State.Value == blnState)
                && m.Url.Contains(keyword));
                int total = Utility.ToTopTotal(count);
                var expressionScalarLambda = GetExpressionScalarLambda();
                return FindListTop(expression, expressionScalarLambda, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Log_WebApiAccessRecord> SearchWebApiAccessRecord(int systemId, string companyId, string startTime, string endTime, string classId, string state, string keyword, int count)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                bool blnState = state.ToBool();
                byte btClassId = classId.ToByte();
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(classId) ? true : m.ClassID.Value == btClassId)
                && (string.IsNullOrEmpty(state) ? true : m.State.Value == blnState)
                && m.Url.Contains(keyword));
                int total = Utility.ToTopTotal(count);
                var expressionScalarLambda = GetExpressionScalarLambda();
                return FindListTop(expression, expressionScalarLambda, m => m.ID, false, total).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountWebApiAccessRecord(int systemId)
        {
            try
            {
                return Count(m => m.SystemID == systemId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountWebApiAccessRecord(int systemId, string companyId)
        {
            try
            {
                return Count(m => m.SystemID == systemId && m.CompanyID == companyId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountWebApiAccessRecord(int systemId, string startTime, string endTime, string classId, string state, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                bool blnState = state.ToBool();
                byte btClassId = classId.ToByte();
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(classId) ? true : m.ClassID.Value == btClassId)
                && (string.IsNullOrEmpty(state) ? true : m.State.Value == blnState)
                && m.Url.Contains(keyword));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int CountWebApiAccessRecord(int systemId, string companyId, string startTime, string endTime, string classId, string state, string keyword)
        {
            try
            {
                DateTime dateStartTime = Utility.ToStartTime(startTime);
                DateTime dateEndTime = Utility.ToEndTime(endTime);
                bool blnState = state.ToBool();
                byte btClassId = classId.ToByte();
                var expression = ExtLinq.True<Ld_Log_WebApiAccessRecord>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId
                && m.CreateDate.Value.Date >= dateStartTime.Date && m.CreateDate.Value.Date <= dateEndTime.Date
                && (string.IsNullOrEmpty(classId) ? true : m.ClassID.Value == btClassId)
                && (string.IsNullOrEmpty(state) ? true : m.State.Value == blnState)
                && m.Url.Contains(keyword));
                return Count(expression);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #region 私有化方法
        private Func<Ld_Log_WebApiAccessRecord, Ld_Log_WebApiAccessRecord> GetExpressionScalarLambda()
        {
            try
            {
                Func<Ld_Log_WebApiAccessRecord, Ld_Log_WebApiAccessRecord> scalarLambda = m => new Ld_Log_WebApiAccessRecord
                {
                    ID = m.ID,
                    SystemID = m.SystemID,
                    CompanyID = m.CompanyID,
                    Account = m.Account,
                    ClassID = m.ClassID,
                    ClassName = m.ClassName,
                    Method = m.Method,
                    Url = m.Url,
                    //Parameter=m.Parameter,
                    Version = m.Version,
                    ControllerName = m.ControllerName,
                    ActionName = m.ActionName,
                    MethodName = m.MethodName,
                    //Result=m.Result,
                    IpAddress = m.IpAddress,
                    State = m.State,
                    CreateDate = m.CreateDate,
                    UpdateDate = m.UpdateDate,
                    TotalMillisecond = m.TotalMillisecond
                };
                return scalarLambda;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


    }
}
