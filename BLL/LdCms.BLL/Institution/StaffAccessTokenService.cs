﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Institution
{
    
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Institution;
    using LdCms.IDAL.Institution;
    using LdCms.Common.Json;
    using LdCms.Common.Extension;

    public partial class StaffAccessTokenService:BaseService<Ld_Institution_StaffAccessToken>,IStaffAccessTokenService
    {
        private readonly IStaffAccessTokenDAL StaffAccessTokenDAL;
        
        public StaffAccessTokenService(IStaffAccessTokenDAL StaffAccessTokenDAL)
        {
            
            this.StaffAccessTokenDAL = StaffAccessTokenDAL;
            this.Dal = StaffAccessTokenDAL;
        }
        public override void SetDal()
        {
            Dal = StaffAccessTokenDAL;
        }

        public bool SaveStaffAccessTokenPro(string accessToken, string refreshToken, int systemId, string companyId, string staffId,string platformId, int expiresIn, int refreshTokenExpiresIn, string ipAddress, int createTimestamp)
        {
            try
            {
                int errCode = -1;
                string errMsg = "fail";
                var result = LdCmsDbEntitiesContext.SP_Add_Institution_StaffAccessToken(accessToken, refreshToken, systemId, companyId, staffId, platformId, expiresIn, refreshTokenExpiresIn, ipAddress, createTimestamp, out errCode, out errMsg);
                if (errCode != 0)
                    throw new Exception(errMsg);
                return errCode == 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool SaveStaffRefreshTokenPro(string verifyRefreshToken, string accessToken, string refreshToken, int expiresIn, int refreshTokenExpiresIn, string ipAddress, int createTimestamp)
        {
            try
            {
                int errCode = -1;
                string errMsg = "fail";
                var result = LdCmsDbEntitiesContext.SP_Add_Institution_StaffRefreshToken(verifyRefreshToken, accessToken, refreshToken, expiresIn, refreshTokenExpiresIn, ipAddress, createTimestamp, out errCode, out errMsg);
                if (errCode != 0)
                    throw new Exception(errMsg);
                return errCode == 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool VerifyStaffAccessTokenPro(string accessToken, int timestamp)
        {
            try
            {
                int errCode = -1;
                string errMsg = "fail";
                var result = LdCmsDbEntitiesContext.SP_Verify_Institution_StaffAccessToken(accessToken, timestamp, out errCode, out errMsg);
                if (errCode != 0)
                    throw new Exception(errMsg);
                return errCode == 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
