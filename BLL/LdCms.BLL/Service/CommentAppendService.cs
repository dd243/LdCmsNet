﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LdCms.BLL.Service
{
    using LdCms.EF.DbModels;
    
    using LdCms.EF.DbStoredProcedure;
    using LdCms.IBLL.Service;
    using LdCms.IDAL.Service;
    using LdCms.Common.Json;
    using LdCms.Common.Security;
    using LdCms.Common.Extension;
    using LdCms.Common.Utility;
    /// <summary>
    /// 
    /// </summary>
    public partial class CommentAppendService:BaseService<Ld_Service_CommentAppend>, ICommentAppendService
    {
        private readonly ICommentAppendDAL CommentappendDAL;
        
        public CommentAppendService(ICommentAppendDAL CommentappendDAL)
        {
            
            this.CommentappendDAL = CommentappendDAL;
            this.Dal = CommentappendDAL;
        }
        public override void SetDal()
        {
            Dal = CommentappendDAL;
        }

        public bool SaveCommentAppend(Ld_Service_CommentAppend entity)
        {
            try
            {
                int systemId = entity.SystemID;
                string companyId = entity.CompanyID;
                string commentId = entity.CommentID;
                string appendId = entity.AppendID;
                if (string.IsNullOrEmpty(appendId))
                {
                    var primarykey = PrimaryKeyHelper.PrimaryKeyType.ServiceCommentAppend;
                    var primaryKeyLen = PrimaryKeyHelper.PrimaryKeyLen.V1;
                    appendId = PrimaryKeyHelper.MakePrimaryKey(primarykey, primaryKeyLen);
                }
                entity.AppendID = appendId;
                entity.State = entity.State.ToBool();
                entity.CreateDate = DateTime.Now;
                return Add(entity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Ld_Service_CommentAppend GetCommentAppend(int systemId, string companyId, string commentId, string appendId)
        {
            try
            {
                return Find(m => m.SystemID == systemId && m.CompanyID == companyId && m.CommentID == commentId && m.AppendID == appendId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Ld_Service_CommentAppend> GetCommentAppend(int systemId, string companyId, string commentId)
        {
            try
            {
                var expression = ExtLinq.True<Ld_Service_CommentAppend>();
                expression = expression.And(m => m.SystemID == systemId && m.CompanyID == companyId && m.CommentID == commentId);
                return FindList(expression, m => m.CreateDate, false).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
