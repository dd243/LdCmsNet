﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.IDAL.Institution
{
    using LdCms.EF.DbModels;
    public partial interface IStaffAccessTokenDAL:IBaseDAL<Ld_Institution_StaffAccessToken>
    {
    }
}
