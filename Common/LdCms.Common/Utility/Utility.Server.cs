﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.Common.Utility
{
    public static partial class Utility
    {
        public static string GetServerMachineName()
        {
            return Environment.MachineName;
        }
        public static string GetNetCoreVersion()
        {
            string runtimeDirectory = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();
            string[] arrRuntimeDirectory = runtimeDirectory.Split('\\');
            int arrTotal = arrRuntimeDirectory.Length;
            return arrRuntimeDirectory[arrTotal - 2];
        }
        /// <summary>
        /// 获取操作系统版本
        /// </summary>
        /// <returns></returns>
        public static string GetSystemVersion()
        {
            return Environment.OSVersion.ToString();
        }                
        /// <summary>       
        /// .NET解释引擎版本       
        /// </summary>        
        /// <returns></returns>        
        public static string GetDotNetVersion()
        {
            return ".NET CLR" + Environment.Version.Major + "." + Environment.Version.Minor + "." + Environment.Version.Build + "." + Environment.Version.Revision;
        }                        
        /// <summary>       
        /// CPU个数            
        /// </summary>        
        /// <returns></returns>        
        public static string GetCpuCount()
        {
            return Environment.GetEnvironmentVariable("NUMBER_OF_PROCESSORS");//CPU个数            
        }         
        /// <summary>        
        /// CPU类型         
        /// </summary>        
        /// <returns></returns>        
        public static string GetCpuIdentifier()
        {
            return Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER");//CPU个数            
        }


    }
}
