﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.Common.Utility
{
    public static partial class Utility
    {

        public static object GetObjectValues(this object obj, string name)
        {
            try
            {
                return obj.GetType().GetProperty(name).GetValue(obj);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
