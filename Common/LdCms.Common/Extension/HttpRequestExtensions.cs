﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.Web;

namespace LdCms.Common.Extension
{
    /// <summary>
    /// 扩展 HttpRequest
    /// </summary>
    public static class HttpRequestExtensions
    {
        /// <summary>
        /// 获取本地IP地址 127.0.0.1
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetLocalIpAddress(this HttpRequest request)
        {
            try
            {
                return request.UserHostAddress.Trim();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取本地IP地址 127.0.0.1
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetRemoteIpAddress(this HttpRequest request)
        {
            try
            {
                return request.UserHostAddress.Trim();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取操作动作 GET、POST
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetHttpMethod(this HttpRequest request)
        {
            try
            {
                return request.HttpMethod.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取内容类型：如：application/json
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetHttpContentType(this HttpRequest request)
        {
            try
            {
                if (request.ContentType == null)
                    return string.Empty;
                return request.ContentType.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取头部值
        /// </summary>
        /// <param name="request"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetHttpHeaderValue(this HttpRequest request, string name)
        {
            try
            {
                return request.Headers[name].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取当前URL 绝对路径 http://www.ldcms.net:80/test/m?id=123
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetAbsoluteUri(this HttpRequest request)
        {
            try
            {
                return request.Url.AbsoluteUri.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取网站网址 http://www.ldcms.net:80
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetHttpWebRoot(this HttpRequest request)
        {
            try
            {
                return new StringBuilder()
                    .Append(request.Url.Scheme)
                    .Append("://")
                    .Append(request.Url.Host)
                    .Append(request.Url.Port)
                    .ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取网站网址 www.ldcms.net
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetHttpWebHost(this HttpRequest request)
        {
            try
            {
                return new StringBuilder()
                    .Append(request.Url.Host)
                    .ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取网站端口 80
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetHttpWebPort(this HttpRequest request)
        {
            try
            {
                return new StringBuilder()
                    .Append(request.Url.Port)
                    .ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取网站路径 /home/index
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetHttpWebPath(this HttpRequest request)
        {
            try
            {
                return new StringBuilder()
                    .Append(request.Path)
                    .ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取网址查询参数 id=123&name=name
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetHttpWebQueryString(this HttpRequest request)
        {
            try
            {
                return new StringBuilder()
                    .Append(request.QueryString)
                    .ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取QueryString
        /// </summary>
        /// <param name="request"></param>
        /// <param name="name">节点名称</param>
        /// <returns></returns>
        public static string GetQueryString(this HttpRequest request, string name)
        {
            try
            {
                var obj = request.QueryString[name];
                if (string.IsNullOrEmpty(obj))
                    return "";
                else
                    return Utility.Utility.FilterText(obj.ToString().Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取FormValue
        /// </summary>
        /// <param name="request"></param>
        /// <param name="name">表单名称</param>
        /// <returns></returns>
        public static string GetFormValue(this HttpRequest request, string name)
        {
            try
            {
                var obj = request.Form[name];
                if (string.IsNullOrEmpty(obj))
                    return "";
                else
                {
                    var sVal = obj.Split(',').FirstOrDefault();
                    return Utility.Utility.FilterText(sVal.ToString().Trim());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取FormValueArr
        /// </summary>
        /// <param name="request"></param>
        /// <param name="name">表单名称</param>
        /// <returns></returns>
        public static string GetFormValueArr(this HttpRequest request, string name)
        {
            try
            {
                var obj = request.Form[name];
                if (string.IsNullOrEmpty(obj))
                    return "";
                else
                    return Utility.Utility.FilterText(obj.ToString().Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取传入流
        /// </summary>
        /// <returns></returns>
        public static string GetInputStream(this HttpRequest request)
        {
            try
            {
                Stream stream = request.InputStream;
                bool isContentLength = request.ContentLength > 0;
                if (!isContentLength)
                    return string.Empty;
                byte[] buffer = new byte[request.InputStream.Length];
                stream.Read(buffer, 0, buffer.Length);
                return Encoding.UTF8.GetString(buffer);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// 获取传入流
        /// </summary>
        /// <param name="request"></param>
        /// <param name="charset"></param>
        /// <returns></returns>
        public static string GetInputStream(this HttpRequest request, string charset)
        {
            try
            {
                Stream stream = request.InputStream;
                bool isContentLength = request.ContentLength > 0;
                if (!isContentLength)
                    return string.Empty;
                byte[] buffer = new byte[request.InputStream.Length];
                stream.Read(buffer, 0, buffer.Length);
                return Encoding.GetEncoding(charset).GetString(buffer);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
