﻿using System;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace LdCms.Common.QRCode
{
    using QRCoder;
    using LdCms.Common.Utility;
    using LdCms.Common.Extension;
    using LdCms.Common.Image;

    /// <summary>
    /// 生成二维码操作类
    /// </summary>
    public static class QRCodeHelper
    {

        public static string ToQRCodeBase64String(string content)
        {
            try
            {
                var image = ToQRCodeBitmap(content, ECCLevel.M, 0, "");
                return ImageHelper.BitmapToBase64String(image);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static string ToQRCodeBase64String(string content, ECCLevel level)
        {
            try
            {
                var image = ToQRCodeBitmap(content, level, 0, "");
                return ImageHelper.BitmapToBase64String(image);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static string ToQRCodeBase64String(string content, ECCLevel level, string iconPaht)
        {
            try
            {
                var image = ToQRCodeBitmap(content, level, 0, iconPaht);
                return ImageHelper.BitmapToBase64String(image);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static byte[] ToQRCodeBinary(string content)
        {
            try
            {
                var image = ToQRCodeBitmap(content, ECCLevel.M, 0, "");
                return GetBitmapByte(image);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static Bitmap ToQRCodeBitmap(string content, ECCLevel level, int iconSize, string iconPaht)
        {
            try
            {
                var slevel = level.ToString();
                int sIconSize = iconSize <= 0 ? 10 : iconSize;
                QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(slevel == "L" ? 0 : slevel == "M" ? 1 : slevel == "Q" ? 2 : 3);
                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(content, eccLevel))
                    {
                        using (var qrCode = new QRCode(qrCodeData))
                        {
                            return qrCode.GetGraphic(20, Color.Black, Color.White, GetIconBitmap(iconPaht), sIconSize);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static Bitmap GetIconBitmap(string iconPaht)
        {
            Bitmap bitmap = null;
            if (!string.IsNullOrEmpty(iconPaht))
            {
                try
                {
                    bitmap = new Bitmap(iconPaht);
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return bitmap;
        }
        private static byte[] GetBitmapByte(Bitmap bitmap)
        {
            try
            {
                System.IO.MemoryStream mstream = new System.IO.MemoryStream();
                bitmap.Save(mstream, System.Drawing.Imaging.ImageFormat.Gif);
                return mstream.ToArray();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private static string SaveImage(Bitmap bitmap, ImageType type,string path)
        {
            try
            {
                System.Drawing.Bitmap bmp = bitmap;
                System.Drawing.Imaging.ImageFormat imageType = System.Drawing.Imaging.ImageFormat.Jpeg;
                if ((int)type == 0)
                    imageType = System.Drawing.Imaging.ImageFormat.Bmp;
                if ((int)type == 1)
                    imageType = System.Drawing.Imaging.ImageFormat.Jpeg;
                if ((int)type == 2)
                    imageType = System.Drawing.Imaging.ImageFormat.Gif;
                if ((int)type == 3)
                    imageType = System.Drawing.Imaging.ImageFormat.Png;
                bmp.Save(path, imageType);
                return path;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public enum ECCLevel
        {
            /// <summary>
            /// 二维码容错率 7%
            /// </summary>
            L = 0,
            /// <summary>
            /// 二维码容错率 15%
            /// </summary>
            M = 1,
            /// <summary>
            /// 二维码容错率 25%
            /// </summary>
            Q = 2,
            /// <summary>
            /// 二维码容错率 30%
            /// </summary>
            H = 3
        }
        public enum ImageType
        {
            Bmp = 0,
            Jpg = 1,
            Gif = 2,
            Png = 3
        }

    }
}