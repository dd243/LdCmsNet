﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.EF.DbStoredProcedure
{
    public class SP_Get_Sell_PayRecordPaging
    {
        public int rownumber { get; set; }
        public decimal TotalAmount { get; set; }
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string wx_trade_no { get; set; }
        public string order_no { get; set; }
        public decimal total_fee { get; set; }
        public decimal order_total_fee { get; set; }
        public bool is_ok { get; set; }
        public DateTime ok_time { get; set; }
        public bool is_err { get; set; }
        public string MemberID { get; set; }
        public string Contacts { get; set; }
        public string Tel { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string HeadImageUrl { get; set; }
        public string Phone { get; set; }
        





    }
}
