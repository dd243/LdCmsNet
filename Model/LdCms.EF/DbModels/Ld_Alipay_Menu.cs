//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LdCms.EF.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ld_Alipay_Menu
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string PlatformID { get; set; }
        public string MenuID { get; set; }
        public string ParentID { get; set; }
        public string MenuKey { get; set; }
        public string Type { get; set; }
        public string ActionType { get; set; }
        public string Name { get; set; }
        public string ActionParam { get; set; }
        public string Icon { get; set; }
        public Nullable<int> Sort { get; set; }
        public Nullable<bool> State { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
