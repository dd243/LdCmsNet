//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LdCms.EF.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ld_Unionpay_AggregatePayTradeOrderRefund
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string OrderID { get; set; }
        public string RefundNo { get; set; }
        public string PlatformID { get; set; }
        public string Service { get; set; }
        public string MchID { get; set; }
        public string TransactionID { get; set; }
        public string OutTradeNo { get; set; }
        public string OutRefundNo { get; set; }
        public string DeviceInfo { get; set; }
        public string StoreID { get; set; }
        public string OperatorID { get; set; }
        public Nullable<int> TotalFee { get; set; }
        public Nullable<int> RefundFee { get; set; }
        public string RefundChannel { get; set; }
        public string Xml { get; set; }
        public string Remark { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
