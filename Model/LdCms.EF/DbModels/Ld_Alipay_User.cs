//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LdCms.EF.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ld_Alipay_User
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string PlatformID { get; set; }
        public string UserID { get; set; }
        public string NickName { get; set; }
        public string Avatar { get; set; }
        public string UserType { get; set; }
        public string UserStatus { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string isStudentCertified { get; set; }
        public string isCertified { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Subscribe { get; set; }
        public Nullable<long> SubscribeTime { get; set; }
        public string SubscribeScene { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
