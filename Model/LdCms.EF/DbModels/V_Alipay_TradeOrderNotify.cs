//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LdCms.EF.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_Alipay_TradeOrderNotify
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string PlatformID { get; set; }
        public string OrderID { get; set; }
        public string NotifyID { get; set; }
        public string NotifyType { get; set; }
        public string Version { get; set; }
        public string Charset { get; set; }
        public string TradeNo { get; set; }
        public string OutTradeNo { get; set; }
        public Nullable<System.DateTime> GmtCreate { get; set; }
        public Nullable<System.DateTime> GmtPayment { get; set; }
        public Nullable<System.DateTime> NotifyTime { get; set; }
        public string Subject { get; set; }
        public string AppID { get; set; }
        public string AuthAppID { get; set; }
        public string SellerID { get; set; }
        public string SellerEmail { get; set; }
        public string BuyerID { get; set; }
        public string BuyerLogonID { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public string ReceiptAmount { get; set; }
        public Nullable<decimal> BuyerPayAmount { get; set; }
        public Nullable<decimal> PointAmount { get; set; }
        public Nullable<decimal> InvoiceAmount { get; set; }
        public string FundBillList { get; set; }
        public string TradeStatus { get; set; }
        public string Sign { get; set; }
        public string SignType { get; set; }
        public string Body { get; set; }
        public string Remark { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
