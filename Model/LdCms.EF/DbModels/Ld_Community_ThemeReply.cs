//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LdCms.EF.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ld_Community_ThemeReply
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string ThemeID { get; set; }
        public string ReplyID { get; set; }
        public string ImgSrc { get; set; }
        public string ImgArray { get; set; }
        public string ReplyContent { get; set; }
        public string ParentReplyID { get; set; }
        public string MemberID { get; set; }
        public string NickName { get; set; }
        public Nullable<int> UpNum { get; set; }
        public Nullable<int> DownNum { get; set; }
        public string IpAddress { get; set; }
        public Nullable<bool> State { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
