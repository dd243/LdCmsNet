﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace LdCms.EF.DbContext
{
    public partial class LdCmsDbEntitiesContext
    {
        public int SP_Add_Institution_StaffAccessToken(string accessToken, string refreshToken, int systemId, string companyId, string staffId, string platformId, int expiresIn, int refreshTokenExpiresIn, string ipAddress, int createTimestamp, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Add_Institution_StaffAccessToken";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@accessToken",SqlDbType.VarChar,64),
                    new SqlParameter("@refreshToken",SqlDbType.VarChar,64),
                    new SqlParameter("@systemId", SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@staffId", SqlDbType.VarChar,20),
                    new SqlParameter("@platformId", SqlDbType.VarChar,32),
                    new SqlParameter("@expiresIn", SqlDbType.Int,4),
                    new SqlParameter("@refreshTokenExpiresIn", SqlDbType.Int,4),
                    new SqlParameter("@ipAddress", SqlDbType.VarChar,20),
                    new SqlParameter("@createTimestamp", SqlDbType.Int,4),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = accessToken;
                param[1].Value = refreshToken;
                param[2].Value = systemId;
                param[3].Value = companyId;
                param[4].Value = staffId;
                param[5].Value = platformId;
                param[6].Value = expiresIn;
                param[7].Value = refreshTokenExpiresIn;
                param[8].Value = ipAddress;
                param[9].Value = createTimestamp;
                param[10].Direction = ParameterDirection.Output;
                param[11].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[10].Value;
                errorMsg = (string)param[11].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SP_Verify_Institution_StaffAccessToken(string accessToken, int timestamp, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Verify_Institution_StaffAccessToken";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@accessToken",SqlDbType.VarChar,64),
                    new SqlParameter("@timestamp", SqlDbType.Int,10),
                    new SqlParameter("@errorCode", SqlDbType.Int,8),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = accessToken;
                param[1].Value = timestamp;
                param[2].Direction = ParameterDirection.Output;
                param[3].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[2].Value;
                errorMsg = (string)param[3].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
