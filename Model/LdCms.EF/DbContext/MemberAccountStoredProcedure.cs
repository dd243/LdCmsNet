﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace LdCms.EF.DbContext
{
    using LdCms.Common.Extension;
    /// <summary>
    /// 会员存储过程
    /// </summary>
    public partial class LdCmsDbEntitiesContext
    {
        public int SP_Add_Member_AccountRegister(int systemId, string companyId, string memberId,int classId,string className, string userName, string password,string name, string phone, string ipAddress, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Add_Member_AccountRegister";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.NVarChar,20),
                    new SqlParameter("@memberId", SqlDbType.VarChar,20),
                    new SqlParameter("@classId", SqlDbType.Int,4),
                    new SqlParameter("@className", SqlDbType.NVarChar,20),
                    new SqlParameter("@userName", SqlDbType.VarChar,32),
                    new SqlParameter("@password", SqlDbType.VarChar,32),
                    new SqlParameter("@name", SqlDbType.NVarChar,20),
                    new SqlParameter("@phone", SqlDbType.VarChar,20),
                    new SqlParameter("@ipAddress", SqlDbType.VarChar,20),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Value = classId;
                param[4].Value = className;
                param[5].Value = userName;
                param[6].Value = password;
                param[7].Value = name;
                param[8].Value = phone;
                param[9].Value = ipAddress;
                param[10].Direction = ParameterDirection.Output;
                param[11].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[10].Value;
                errorMsg = (string)param[11].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SP_Update_Member_AccountState(int systemId, string companyId, string memberId,bool state, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Update_Member_AccountState";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@memberId", SqlDbType.VarChar,20),
                    new SqlParameter("@state", SqlDbType.Bit,8),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Value = state;
                param[4].Direction = ParameterDirection.Output;
                param[5].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[4].Value;
                errorMsg = (string)param[5].Value;
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SP_Update_Member_AccountDelete(int systemId, string companyId, string memberId, bool delete, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Update_Member_AccountDelete";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@memberId", SqlDbType.VarChar,20),
                    new SqlParameter("@delete", SqlDbType.Bit,8),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Value = delete;
                param[4].Direction = ParameterDirection.Output;
                param[5].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[4].Value;
                errorMsg = (string)param[5].Value;
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SP_Update_Member_AccountPassword(int systemId, string companyId, string memberId, string password, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Update_Member_AccountPassword";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@memberId", SqlDbType.VarChar,20),
                    new SqlParameter("@password", SqlDbType.VarChar,32),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Value = password;
                param[4].Direction = ParameterDirection.Output;
                param[5].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[4].Value;
                errorMsg = (string)param[5].Value;
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SP_Update_Member_AccountPhone(int systemId, string companyId, string memberId, string phone, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Update_Member_AccountPhone";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@memberId", SqlDbType.VarChar,20),
                    new SqlParameter("@phone", SqlDbType.VarChar,32),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Value = phone;
                param[4].Direction = ParameterDirection.Output;
                param[5].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[4].Value;
                errorMsg = (string)param[5].Value;
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SP_Delete_Member_Account(int systemId, string companyId, string memberId, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Delete_Member_Account";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@memberId", SqlDbType.VarChar,20),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Direction = ParameterDirection.Output;
                param[4].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[3].Value;
                errorMsg = (string)param[4].Value;
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ArrayList SP_Get_Member_Account(int systemId, string companyId, string memberId, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Get_Member_Account";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.NVarChar,20),
                    new SqlParameter("@memberId", SqlDbType.NVarChar,20),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = memberId;
                param[3].Direction = ParameterDirection.Output;
                param[4].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[3].Value;
                errorMsg = (string)param[4].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ArrayList SP_Get_Member_AccountByAccessToken(int systemId,string accessToken, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Get_Member_AccountByAccessToken";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@accessToken", SqlDbType.NVarChar,64),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = accessToken;
                param[2].Direction = ParameterDirection.Output;
                param[3].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[2].Value;
                errorMsg = (string)param[3].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ArrayList SP_Get_Member_AccountByRefreshToken(int systemId, string refreshToken, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Get_Member_AccountByRefreshToken";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@refreshToken", SqlDbType.NVarChar,64),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = refreshToken;
                param[2].Direction = ParameterDirection.Output;
                param[3].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[2].Value;
                errorMsg = (string)param[3].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ArrayList SP_Get_Member_AccountTop(int systemId, string companyId, string delete, int count, out int errorCode, out string errorMsg, out int rowCount)
        {
            try
            {
                string cmdText = "SP_Get_Member_AccountTop";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId", SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@delete", SqlDbType.VarChar,8),
                    new SqlParameter("@count", SqlDbType.Int,4),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400),
                    new SqlParameter("@rowCount", SqlDbType.Int,4)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = delete;
                param[3].Value = count;
                param[4].Direction = ParameterDirection.Output;
                param[5].Direction = ParameterDirection.Output;
                param[6].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[4].Value;
                errorMsg = (string)param[5].Value;
                rowCount = (int)param[6].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ArrayList SP_Get_Member_AccountPaging(int systemId, string companyId, string delete, int pageId, int pageSize, out int errorCode, out string errorMsg, out int rowCount)
        {
            try
            {
                string cmdText = "SP_Get_Member_AccountPaging";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId", SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.VarChar,20),
                    new SqlParameter("@delete", SqlDbType.VarChar,8),
                    new SqlParameter("@pageId", SqlDbType.Int,4),
                    new SqlParameter("@pageSize", SqlDbType.Int,4),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400),
                    new SqlParameter("@rowCount", SqlDbType.Int,4)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = delete;
                param[3].Value = pageId;
                param[4].Value = pageSize;
                param[5].Direction = ParameterDirection.Output;
                param[6].Direction = ParameterDirection.Output;
                param[7].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[5].Value;
                errorMsg = (string)param[6].Value;
                rowCount = (int)param[7].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ArrayList SP_Search_Member_Account(int systemId, string companyId, string startTime, string endTime,string classId, string rankId, string keyword, string delete, out int errorCode, out string errorMsg, out int rowCount)
        {
            try
            {
                string cmdText = "SP_Search_Member_Account";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.NVarChar,20),
                    new SqlParameter("@startTime", SqlDbType.NVarChar,20),
                    new SqlParameter("@endTime", SqlDbType.NVarChar,20),
                    new SqlParameter("@classId", SqlDbType.NVarChar,20),
                    new SqlParameter("@rankId", SqlDbType.NVarChar,20),
                    new SqlParameter("@keyword", SqlDbType.NVarChar,100),
                    new SqlParameter("@delete", SqlDbType.NVarChar,8),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,200),
                    new SqlParameter("@rowCount", SqlDbType.Int,8)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = startTime;
                param[3].Value = endTime;
                param[4].Value = classId;
                param[5].Value = rankId;
                param[6].Value = keyword;
                param[7].Value = delete;
                param[8].Direction = ParameterDirection.Output;
                param[9].Direction = ParameterDirection.Output;
                param[10].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[8].Value;
                errorMsg = (string)param[9].Value;
                rowCount = (int)param[10].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ArrayList SP_Search_Member_AccountTop(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, string delete, int count, out int errorCode, out string errorMsg, out int rowCount)
        {
            try
            {
                string cmdText = "SP_Search_Member_AccountTop";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.NVarChar,20),
                    new SqlParameter("@startTime", SqlDbType.NVarChar,20),
                    new SqlParameter("@endTime", SqlDbType.NVarChar,20),
                    new SqlParameter("@classId", SqlDbType.NVarChar,20),
                    new SqlParameter("@rankId", SqlDbType.NVarChar,20),
                    new SqlParameter("@keyword", SqlDbType.NVarChar,100),
                    new SqlParameter("@delete", SqlDbType.NVarChar,8),
                    new SqlParameter("@count",SqlDbType.Int,4),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,200),
                    new SqlParameter("@rowCount", SqlDbType.Int,8)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = startTime;
                param[3].Value = endTime;
                param[4].Value = classId;
                param[5].Value = rankId;
                param[6].Value = keyword;
                param[7].Value = delete;
                param[8].Value = count;
                param[9].Direction = ParameterDirection.Output;
                param[10].Direction = ParameterDirection.Output;
                param[11].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[9].Value;
                errorMsg = (string)param[10].Value;
                rowCount = (int)param[11].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public ArrayList SP_Search_Member_AccountPaging(int systemId, string companyId, string startTime, string endTime, string classId, string rankId, string keyword, string delete, int pageId, int pageSize, out int errorCode, out string errorMsg, out int rowCount)
        {
            try
            {
                string cmdText = "SP_Search_Member_AccountPaging";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.NVarChar,20),
                    new SqlParameter("@startTime", SqlDbType.NVarChar,20),
                    new SqlParameter("@endTime", SqlDbType.NVarChar,20),
                    new SqlParameter("@classId", SqlDbType.NVarChar,20),
                    new SqlParameter("@rankId", SqlDbType.NVarChar,20),
                    new SqlParameter("@keyword", SqlDbType.NVarChar,100),
                    new SqlParameter("@delete", SqlDbType.NVarChar,8),
                    new SqlParameter("@pageId",SqlDbType.Int,4),
                    new SqlParameter("@pageSize",SqlDbType.Int,4),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,200),
                    new SqlParameter("@rowCount", SqlDbType.Int,8)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = startTime;
                param[3].Value = endTime;
                param[4].Value = classId;
                param[5].Value = rankId;
                param[6].Value = keyword;
                param[7].Value = delete;
                param[8].Value = pageId;
                param[9].Value = pageSize;
                param[10].Direction = ParameterDirection.Output;
                param[11].Direction = ParameterDirection.Output;
                param[12].Direction = ParameterDirection.Output;
                var result = this.ExecuteReaderPro(cmdText, param);
                errorCode = (int)param[10].Value;
                errorMsg = (string)param[11].Value;
                rowCount = (int)param[112].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int SP_Verify_Member_AccountLogin(int systemId, string companyId, string account, string password, out int errorCode, out string errorMsg)
        {
            try
            {
                string cmdText = "SP_Verify_Member_AccountLogin";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@systemId",SqlDbType.Int,4),
                    new SqlParameter("@companyId", SqlDbType.NVarChar,20),
                    new SqlParameter("@account", SqlDbType.VarChar,20),
                    new SqlParameter("@password", SqlDbType.VarChar,32),
                    new SqlParameter("@errorCode", SqlDbType.Int,4),
                    new SqlParameter("@errorMsg", SqlDbType.NVarChar,400)
                };
                param[0].Value = systemId;
                param[1].Value = companyId;
                param[2].Value = account;
                param[3].Value = password;
                param[4].Direction = ParameterDirection.Output;
                param[5].Direction = ParameterDirection.Output;
                var result = this.ExecuteNonQueryPro(cmdText, param);
                errorCode = (int)param[4].Value;
                errorMsg = (string)param[5].Value;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
