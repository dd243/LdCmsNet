﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LdCms.Model.Comment
{
    public class ServiceComment
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string CommentID { get; set; }
        public string MemberID { get; set; }
        public string OrderID { get; set; }
        public int? ServiceStatus { get; set; }
        public int? ServicePoints { get; set; }
        public string Images { get; set; }
        public string Description { get; set; }
        public int? UpNum { get; set; }
        public int? DownNum { get; set; }
        public bool? State { get; set; }
        public DateTime? CreateDate { get; set; }
        public List<ServiceCommentAppend> Details { get; set; }
    }
    public partial class ServiceCommentAppend
    {
        public int SystemID { get; set; }
        public string CompanyID { get; set; }
        public string CommentID { get; set; }
        public string AppendID { get; set; }
        public string Images { get; set; }
        public string Description { get; set; }
        public bool? State { get; set; }
        public DateTime? CreateDate { get; set; }
    }

}
